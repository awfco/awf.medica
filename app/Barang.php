<?php namespace App;

use App\ConfHarga;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'jenis', 'harga', 'satuan', 'tanggal_kadaluarsa', 'stock', 'update_by', 'status'];

	public function total($dokter = null){
		if(!$dokter){
			$confHargas = ConfHarga::where(['status' => true, 'is_non_resep' => true])->get();
			$total = count($confHargas) - 1;
			if($total >= 0){
				foreach($confHargas as $key => $confHarga){
					if($this->harga <= $confHarga->max || $key == $total){
						$margin = $confHarga->margin;
						break;
					}
				}
			}else{
				$margin = 0;
			}
		}else{
			$margin = $dokter->confHarga->margin;
		}
		return ($this->harga + ($this->harga * $margin));
	}
}

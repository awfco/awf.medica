<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfHarga extends Model {

	//
	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'margin', 'update_by', 'status', 'is_non_resep', 'max'];

  public static function boot(){
		parent::boot();
		
		self::saving(function ($model){
			if($model->max == '') $model->max = NULL;
		});
	}
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LaporanDetailPenjualan;

class DetailPenjualan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['penjualan_id', 'barang_id', 'racikan_id', 'total', 'disc', 'update_by'];

	public function penjualan(){
		return $this->belongsTo('App\Penjualan');
	}

	public function barang(){
		return $this->belongsTo('App\Barang');
	}

	public function racikan(){
		return $this->belongsTo('App\Racikan');
	}

	public function laporanDetailPenjualan(){
		return LaporanDetailPenjualan::where(['no_nota' => $this->penjualan->no_nota, 'barang_id' => $this->barang_id])->first();
	}
}

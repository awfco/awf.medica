<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailRacikan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['racikan_id', 'barang_id', 'qty', 'update_by', 'status'];

	public function barang(){
        	return $this->belongsTo('App\Barang');
	}

	public function racikan(){
		return $this->belongsTo('App\Racikan');
	}

	public function harga(){
		return ($this->barang->harga + ($this->barang->harga * $this->racikan->dokter->confHarga->margin)) * $this->qty;
	}

}

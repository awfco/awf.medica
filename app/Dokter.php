<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'spesialis', 'alamat', 'notelp', 'conf_harga_id', 'update_by', 'status'];

    public function confHarga()
    {
        return $this->belongsTo('App\ConfHarga');
    }

}

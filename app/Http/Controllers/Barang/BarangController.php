<?php namespace App\Http\Controllers\Barang;

use App\Barang;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class BarangController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data['barang'] = Barang::where('status', true)->get();
		return view('setup/barang', $data);
	}

	public function getAll()
	{
		$data['barang'] = Barang::where('status', true)->get();
		return view('setup/barangTable', $data);
	}

	public function getDataOne($id)
	{
		return Barang::where('id', $id)->first();
	}

	public function postDelete(Request $request)
	{
		$barang = Barang::find($request['id']);
		if($barang->update(['update_by' => 1, 'status' => false]))
			return $barang;
		else
			return false;
	}

	public function postUpdate(Request $request)
	{
		$barang = Barang::find($request['id']);
		if($barang->update($request->all() + ['update_by' => 1]))
			return $barang;
		else
			return false;
	}

	public function postInsert(Request $request)
	{
		if($barang = Barang::create($request->all() + ['update_by' => 1])){
			return $barang;
		}else{
			return false;
		}

	}
}

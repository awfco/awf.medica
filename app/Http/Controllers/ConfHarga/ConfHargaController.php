<?php namespace App\Http\Controllers\ConfHarga;

use App\ConfHarga;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ConfHargaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data['confharga'] = ConfHarga::where(array('status' => true, 'is_non_resep' => false))->get();
		$data['hargaNonResep'] = ConfHarga::where(array('status' => true, 'is_non_resep' => true))->orderBy('max')->get();
		return view('setup/confharga', $data);
	}

	public function getAll()
	{
		$data['confharga'] = ConfHarga::where(array('status' => true, 'is_non_resep' => false))->get();
		$data['hargaNonResep'] = ConfHarga::where(array('status' => true, 'is_non_resep' => true))->orderBy('max')->get();
		return view('setup/confhargaTable', $data);
	}

	public function getDataOne($id)
	{
		return ConfHarga::where('id', $id)->where('status', true)->first();
	}

	public function postDelete(Request $request)
	{
		$ConfHarga = ConfHarga::find($request['id']);
		if($ConfHarga->update(['update_by' => 1, 'status' => false]))
			return $ConfHarga;
		else
			return false;
	}

	public function postUpdate(Request $request)
	{
		$ConfHarga = ConfHarga::find($request['id']);
		if($ConfHarga->update($request->all() + ['update_by' => 1]))
			return $ConfHarga;
		else
			return false;
	}

	public function getSetNonResep($id)
	{
		$old = ConfHarga::where(array('status' => true, 'is_non_resep' => true))->first();
		if(!$old){
			$old->is_non_resep = false;
			if(!$old->update()){
				return false;
			}
		}
		$new = ConfHarga::find($id);
		$new->is_non_resep = true;
		if($new->update()){
			return true;
		}else{
			return false;
		}
	}

	public function postInsert(Request $request)
	{
		if($ConfHarga = ConfHarga::create($request->except('created_at') + ['update_by' => 1])){
			return $ConfHarga;
		}else{
			return false;
		}

	}

}

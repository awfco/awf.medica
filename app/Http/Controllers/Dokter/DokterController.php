<?php namespace App\Http\Controllers\Dokter;

use App\Dokter;
use App\ConfHarga;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DokterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data['dokter'] = Dokter::where('status', true)->whereHas('confHarga', function($query){
			$query->where('status', true);
		})->get();
		$data['confharga'] = $this->getConfHargaList();
		return view('setup/dokter', $data);
	}

	public function getAllDokter()
	{
		$data['dokter'] = Dokter::where('status', true)->whereHas('confHarga', function($query){
			$query->where('status', true);
		})->get();
		return view('setup/dokterTable', $data);
	}

	public function getDataOne($id)
	{
		return Dokter::where('id', $id)->first();
	}

	public function postDelete(Request $request)
	{
		$dokter = Dokter::find($request['id']);
		if($dokter->update(['update_by' => 1, 'status' => false]))
			return $dokter;
		else
			return false;
	}

	public function postUpdate(Request $request)
	{
		$dokter = Dokter::find($request['id']);
		if($dokter->update($request->all() + ['update_by' => 1]))
			return $dokter;
		else
			return false;
	}

	public function postInsert(Request $request)
	{
		if($dokter = Dokter::create($request->all() + ['update_by' => 1])){
			return $dokter;
		}else{
			return false;
		}

	}

	public function getConfHargaList()
	{
		return ConfHarga::where(['status' => true, 'is_non_resep' => false])->get();
	}

}

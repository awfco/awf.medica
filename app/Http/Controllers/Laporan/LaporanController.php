<?php namespace App\Http\Controllers\Laporan;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LaporanPenjualan;
use App\LaporanDetailPenjualan;
use App\LaporanReturPenjualan;
use App\Neraca;

use Illuminate\Http\Request;

class LaporanController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getPenjualan()
	{
		$data["laporans"] = LaporanPenjualan::all();
		return view('laporan/penjualan', $data);
	}

	public function getFeedokter()
	{
		$data["laporans"] = LaporanPenjualan::where('jenis', 'Resep')->get();
		return view('laporan/feedokter', $data);
	}

	public function getBarang()
	{
		$data["laporans"] = LaporanDetailPenjualan::all();
		return view('laporan/barang', $data);	
	}

	public function getLabarugi()
	{
		$data["laporans"] = LaporanDetailPenjualan::all();
		return view('laporan/labarugi', $data);
	}
	
	public function getCashflow()
	{
		$data["laporans"] = Neraca::all();
		return view('laporan/cashflow', $data);
	}

	public function getReturpenjualan()
	{
		$data["laporans"] = LaporanReturPenjualan::all();
		return view('laporan/returpenjualan', $data);
	}



}

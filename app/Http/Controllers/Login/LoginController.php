<?php namespace App\Http\Controllers\Login;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\User;

class LoginController extends Controller {
	public function getLogin(){
		if(Session::has('username')) return redirect('penjualan/transaksi');
		return view('login');
	}

	public function postProses(Request $request){
		$username = $request->all()['username'];
		$password = $request->all()['password'];
		$user = User::where(['username' => $username, 'password' => md5($password)])->first();
		if($user){
			Session::put('username', $username);
			Session::put('role', $user->role);
			return json_encode(true);
		}else{
			return json_encode(false);
		}
	}

	public function getLogout(){
		Session::flush();
		return redirect('/auth/login');
	}
}

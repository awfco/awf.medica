<?php namespace App\Http\Controllers\Pasien;

use App\Pasien;
use App\Dokter;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PasienController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data['pasien'] = Pasien::where('status', true)->whereHas('dokter', function($query){
			$query->where('status', true)->whereHas('confHarga', function($q){
				$q->where('status', true);
			});
		})->get();
		$data['dokter'] = Dokter::where('status', true)->whereHas('confHarga', function($q){
			$q->where('status', true);
		})->get();
		return view('setup/pasien', $data);
	}

	public function getAll()
	{
		$data['pasien'] = Pasien::where('status', true)->whereHas('dokter', function($query){
			$query->where('status', true)->whereHas('confHarga', function($q){
				$q->where('status', true);
			});
		})->get();
		return view('setup/pasienTable', $data);
	}

	public function getDataOne($id)
	{
		return Pasien::where('id', $id)->first();
	}

	public function postDelete(Request $request)
	{
		$pasien = Pasien::find($request['id']);
		if($pasien->update(['update_by' => 1, 'status' => false]))
			return $pasien;
		else
			return false;
	}

	public function postUpdate(Request $request)
	{
		$pasien = Pasien::find($request['id']);
		if($pasien->update($request->all() + ['update_by' => 1]))
			return $pasien;
		else
			return false;
	}

	public function postInsert(Request $request)
	{
		if($pasien = Pasien::create($request->all() + ['update_by' => 1])){
			return $pasien;
		}else{
			return false;
		}

	}

}

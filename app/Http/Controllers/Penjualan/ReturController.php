<?php namespace App\Http\Controllers\Penjualan;

use App\Barang;
use App\DetailPenjualan;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LaporanDetailPenjualan;
use App\LaporanPenjualan;
use App\LaporanReturPenjualan;
use App\Neraca;
use App\Penjualan;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;

class ReturController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data["penjualans"] = Penjualan::whereHas('detailPenjualan.barang', function($query){
			$query->where('barangs.status', 1);
		})
		->where(function($z){
			$z->whereHas('pasien', function($query){
				$query->where('status', 1)->whereHas('dokter', function($q){
					$q->where('dokters.status', 1);
				});
			})
			->oRWhere('pasien_id', null);
		})
		->orderBy('created_at', 'desc')->get();
		$data["data"] = "";
		return view('penjualan/retur', $data);
	}

	public function getOpenmodal($id)
	{
		$data["data"] = Penjualan::with('detailPenjualan')
		->where('penjualans.id', $id)
		->whereHas('detailPenjualan.barang', function($query){
			$query->where('barangs.status', 1);
		})
		->where(function($z){
			$z->whereHas('pasien', function($query){
				$query->where('status', 1)->whereHas('dokter', function($q){
					$q->where('dokters.status', 1);
				});
			})
			->oRWhere('pasien_id', null);
		})
		->orderBy('created_at', 'desc')->first();
		return view('penjualan/returModal', $data);
	}

	public function postReturbarang(Request $request)
	{
		$data = $request->all();
		$idPenjualan = $data['id'];
		$penjualan = Penjualan::find($idPenjualan);
		$returnedBarang = $data['barang_id'];
		$qtyreturned = $data['qty_barang'];
		$neraca = new Neraca();
		$neraca->kode_transaksi = $penjualan->no_nota;
		$neraca->user_id = 1;
		$neraca->tgl_transaksi = date('Y-m-d');
		foreach($returnedBarang as $dtl){
			$detailPenjualan = DetailPenjualan::find($dtl);
			$barang = Barang::find($detailPenjualan->barang_id);
			$barang->stock = $barang->stock + $qtyreturned[$dtl];
			$barang->update();
			if($detailPenjualan){
				$detailPenjualan->total = $detailPenjualan->total - $qtyreturned[$dtl];
				$detailPenjualan->update();
			}
			$laporanDetailPenjualan = LaporanDetailPenjualan::where(['no_nota' => $penjualan->no_nota, 'barang_id' => $barang->id])->first();
			$laporanDetailPenjualan->qty = $laporanDetailPenjualan->qty - $qtyreturned[$dtl];
			$laporanretur = new LaporanReturPenjualan();
			$laporanretur->no_nota = $penjualan->no_nota;
			$laporanretur->nama_barang = $barang->nama;
			$laporanretur->qty = $qtyreturned[$dtl];
			$laporanretur->jumlah = ($data['returtype'] == 0) ? $laporanDetailPenjualan->harga_jual * $laporanretur->qty : $laporanDetailPenjualan->harga_beli * $laporanretur->qty;
			$laporanretur->update_by = 1;
			$laporanretur->save();
			$laporanDetailPenjualan->update();
		}
		$laporanPenjualan = LaporanPenjualan::where('no_nota', $penjualan->no_nota)->first();
		$laporanPenjualan->total_tunai = $laporanPenjualan->total_tunai - $data['totalretur'];
		$laporanPenjualan->update();
		$neraca->total = $data['totalretur'] * -1;
		$neraca->deskripsi = "Retur obat";
		$neraca->save();
	}

}

<?php namespace App\Http\Controllers\Penjualan;

use App\Penjualan;
use App\DetailPenjualan;
use App\LaporanPenjualan;
use App\LaporanDetailPenjualan;
use App\Neraca;
use App\Pasien;
use App\Dokter;
use App\Barang;
use App\Racikan;
use App\RacikanTransaksi;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mike42\Escpos\Printer; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

use Illuminate\Http\Request;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class TransaksiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data["today"] = date('d-m-Y');
		$data["todaydatetime"] = date('d-m-Y H:i');
		$todayTransaction = Penjualan::where('tgl_penjualan', date('Y-m-d')." 00:00:00")->count();
		$data["nonota"] = date('d/m/y')."/".str_pad(($todayTransaction+1), 3, "0", STR_PAD_LEFT);
		$data["dokter"] = Dokter::where('status', true)->whereHas('confHarga', function($q){
			$q->where('status', true);
		})->get();
		$data["pasien"] = [];
		$data["dkt"] = "";
		$data["dkt_id"] = "";
		$data["barang"] = Barang::where(['status' => true])->where('stock', '>', 0)->get();
		$data["racikan"] = [];
		$data["picked_dokter"] = null;
		return view('penjualan/transaksi', $data);
	}

	public function getBarang($id, $isdinamis)
	{
		$data["picked_dokter"] = ($id == 0) ? null : Dokter::find($id);
		$data["margin"] = ($id == 0) ? 0 : $data["picked_dokter"]->confHarga->margin;
		$data["barang"] = Barang::where(['status' => true])->where('stock', '>', 0)->get();
		$data["racikan"] = ($id == 0 || $isdinamis == "1") ? [] : Racikan::where('status', true)
		->whereHas('dokter', function($query) use($id){
			$query->where(['status' => true, 'id' => $id]);
		})
		->whereHas('dokter.confHarga', function($query) use($id){
			$query->where('status', true);
		})
		->whereHas('detailRacikan', function($q){
			$q->whereHas('barang', function($qu){
				$qu->where('stock', '>', 0);
			}, '=', 0);
		}, '=', 0)
		->get();
		return view('penjualan/transaksiBarang', $data);
	}

	public function getPasien($id)
	{
		$data["today"] = date('d-m-Y');
		$data["todaydatetime"] = date('d-m-Y H:i');
		$todayTransaction = Penjualan::where('tgl_penjualan', $data["today"])->count();
		$data["nonota"] = date('d/m/y')."/".str_pad(($todayTransaction+1), 3, "0", STR_PAD_LEFT);
		$data["dokter"] = Dokter::where('status', true)->whereHas('confHarga', function($q){
			$q->where('status', true);
		})->get();
		$data["barang"] = Barang::where(['status' => true])->get();
		$data["racikan"] = [];
		$data["picked_dokter"] = null;
		$data["pasien"] = Pasien::where(['status' => true, 'dokter_id' => $id])->with('dokter')->get();
		$data["dkt"] = Dokter::find($id)->nama;
		$data["dkt_id"] = $id;
		return view('penjualan/transaksiPasien', $data);
	}

	public function postBayar(Request $request)
	{
		$state = "nonRacikan";
		$iRacik = 0;
		$data = $request->all();
		$penjualan = new Penjualan();
		$laporanPenjualan = new LaporanPenjualan();
		$neraca = new Neraca();
		$penjualan->no_nota = $data["nonota"];
		$laporanPenjualan->no_nota = $data["nonota"];
		$neraca->kode_transaksi = $data["nonota"];
		$neraca->deskripsi = "Penjualan obat";
		$neraca->tgl_transaksi = date_create_from_format('d-m-Y',$data["tglTransaksi"])->format("Y-m-d");
		$penjualan->tgl_penjualan = date_create_from_format('d-m-Y',$data["tglTransaksi"])->format("Y-m-d");
		$laporanPenjualan->tgl_penjualan = date_create_from_format('d-m-Y',$data["tglTransaksi"])->format("Y-m-d");
		$penjualan->jenis = $data["jenis"];
		$laporanPenjualan->jenis = $data["jenis"];
		$laporanPenjualan->dokter = ($data["jenis"]=="Resep")?Dokter::find($data["dokter_id"])->nama:null;
		$penjualan->pasien_id = (array_key_exists("pasien_id", $data))?$data["pasien_id"]:null;
		$laporanPenjualan->pasien = (array_key_exists("nama", $data))?$data["nama"]:null;
		$penjualan->ppn = $data["ppn"];
		$laporanPenjualan->ppn = $data["ppn"];
		$penjualan->disc = $data["disc"];
		$laporanPenjualan->diskon = $data["disc"];
		$laporanPenjualan->total_tunai = $data["grandtotal"] - $data["total_debit"];
		$laporanPenjualan->total_debit = $data["total_debit"];
		$laporanPenjualan->bank_debit = ($data["total_debit"] > 0)?$data["bank_debit"]:"";
		$penjualan->update_by = 1;
		$penjualan->save();
		$penjualanId = $penjualan->id;
		$total_dokter = 0;

		$textprint = str_pad ( "No nota" , 10 , " ").":".$data["nonota"] . "\n";
		$textprint .= str_pad ( "Dokter" , 10 , " ").":".(($data["jenis"]=="Resep") ? $laporanPenjualan->dokter : "-") . "\n";
		$textprint .= str_pad ( "Pasien" , 10 , " ").":".(($data["jenis"]=="Resep")?$laporanPenjualan->pasien:"Umum") . "\n";
		$textprint .= str_pad ( "" , 32 , "=")."\n";
		$textprint .= str_pad ( "Deskripsi" , 20 , " ").str_pad ( "Qty" , 3 , " ").str_pad ( "Total" , 9 , " ", STR_PAD_LEFT)."\n";
		$textprint .= str_pad ( "" , 32 , "=")."\n";
		
		$totalData = sizeof($data["barang"]) - 1;
		for($i = 0; $i < $totalData; $i++){
			if($state == "racikan"){
				if($data["type"][$i] != "nonRacikanRacikan"){
					$state = "nonRacikan";
					$namaRacikan = "";
				}else{
					$brg = Barang::find($data["barang"][$i]);
					$laporanDetail = new LaporanDetailPenjualan();
					$laporanDetail->no_nota = $data["nonota"];
					$laporanDetail->nama_barang = $namaRacikan.$brg->nama;
					$laporanDetail->qty = $data["qty"][$i];
					$laporanDetail->harga_beli = $brg->harga;
					$laporanDetail->harga_jual = $brg->total(($data["jenis"]=="Resep")?Dokter::find($data["dokter_id"]):null);
					$laporanDetail->diskon = $data["discOne"][$i];
					$laporanDetail->save();
					$racikTrans = new RacikanTransaksi();
					$racikTrans->detail_penjualan_id = $detailOld->id;
					$racikTrans->racikan_id = $detailOld->racikan_id;
					$racikTrans->barang_id = $data["barang"][$i];
					$racikTrans->total = $data["qtyOne"][$iRacik] * $detailOld->total;
					$racikTrans->save();
					$brg->stock = $brg->stock - $data["qty"][$i];
					$brg->save();
					$iRacik += 1;
				}
			}
			if($state == "nonRacikan"){
				$detail = new DetailPenjualan();
				$detail->penjualan_id = $penjualanId;
				$detail->total = $data["qty"][$i];
				$detail->disc = $data["discOne"][$i];
				$detail->update_by = 1;
				if($data["type"][$i] == "racikan"){
					$brg = Racikan::find($data["barang"][$i]);
					$total_dokter = $total_dokter + (Racikan::find($data["barang"][$i])->fee_dokter * $detail->total);
					$detail->racikan_id = $data["barang"][$i];
					$textprint .= str_pad ( $brg->nama , 20 , " ").str_pad ($data['qty'][$i] , 3 , " ").str_pad ( number_format(($brg->total()*$data['qty'][$i]), 0, ',', '.') , 9 , " ", STR_PAD_LEFT)."\n";
					$state = "racikan";
					$namaRacikan = $brg->nama." | ";
				}else{
					$brg = Barang::find($data["barang"][$i]);
					$laporanDetail = new LaporanDetailPenjualan();
					$laporanDetail->no_nota = $data["nonota"];
					$laporanDetail->barang_id = $data["barang"][$i];
					$laporanDetail->nama_barang = $brg->nama;
					$laporanDetail->qty = $data["qty"][$i];
					$laporanDetail->harga_beli = $brg->harga;
					$laporanDetail->harga_jual = $brg->total(($data["jenis"]=="Resep")?Dokter::find($data["dokter_id"]):null);
					$textprint .= str_pad ( $brg->nama , 20 , " ").str_pad ( $data['qty'][$i] , 3 , " ").str_pad ( number_format(($laporanDetail->harga_jual *  $data["qty"][$i]), 0, ',', '.') , 9 , " ", STR_PAD_LEFT)."\n";
					$laporanDetail->diskon = $data["discOne"][$i];
					$laporanDetail->save();
					$detail->barang_id = $data["barang"][$i];
					$brg->stock = $brg->stock - $data["qty"][$i];
					$brg->save();
				}
				$detail->save();
				$detailOld = $detail;
			}
		}


		$textprint .= str_pad ( "" , 32 , "=")."\n";
		$textprint .= str_pad ( "Total" , 32-9 , " ").str_pad ( number_format($data['grandtotal'], 0, ',', '.') , 9 , " ", STR_PAD_LEFT). "\n";
		$textprint .= str_pad ( "" , 32 , "=")."\n";
		$textprint .= "Terima kasih lekas sembuh";

		$neraca->user_id = 1;
		$neraca->total = $data["grandtotal"];
		$neraca->save();

		$laporanPenjualan->total_dokter = $total_dokter;
		$laporanPenjualan->save();

		if($data['print'] > 0){
			$ip = '127.0.0.1'; // IP Komputer kita atau printer lain yang masih satu jaringan
			$printername = 'POS-58'; // Nama Printer yang di sharing
			$connector = new WindowsPrintConnector("smb://" . $ip . "/" . $printername);
			$printer = new Printer($connector);
		}

		for($x = 0; $x < $data['print']; $x++) {
			// printer
			$printer->setJustification(Printer::JUSTIFY_CENTER);
			$printer -> text("APOTEK PRAMUKA" . "\n");
			$printer -> text("Jl. Pramuka no 211" . "\n");
			$printer -> text("0812323123" . "\n");
			$printer -> text(date('d-m-Y') . "\n");
			$printer -> text(str_pad ( "" , 32 , "=")."\n");
			$printer -> setJustification(Printer::JUSTIFY_LEFT);
			$printer -> text($textprint);
			$printer -> feed(3);
			if($x == $data['print'] - 1){
				$printer -> close();
			}
		}
	}

}

<?php namespace App\Http\Controllers\Profil;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\ProfilApotek;
use Illuminate\Support\Facades\URL;

class ApotekController extends Controller {

	public function getIndex(){
		$data['apotek'] = ProfilApotek::first();
		return view('setup/profilapotek', $data);
	}

	public function postChange(Request $request){
		$data = $request->all();
		$profil = ProfilApotek::first();
		if(!$profil) $profil = new ProfilApotek;
		$profil->nama = $data['nama'];
		$profil->alamat = $data['alamat'];
		$profil->notelp = $data['notelp'];
		$profil->footernota = $data['footernota'];
		$profil->update_by = 1;
		if($profil->save()){
			Session::put('apotekname', $data['nama']);
			return json_encode(array('success' => true));
		}
		return json_encode(array('success' => false, 'msg' => 'Gagal ganti data profil apotek'));
	}

	public function postChangepp(Request $request){
		$img = $request->file('pp');
		if($img->getSize()/1000 > 2054){
			return json_encode(array('success' => false, 'msg' => 'Ukuran gambar terlalu besar('.($img->getSize()/1000).'), max 2Mb'));
		}
		$destination = 'uploads/pp/';
		$profil = ProfilApotek::first();
		if(!$profil) $profil = new ProfilApotek;
		if($img->move($destination, 'pp.jpg')){
			$profil->logo = $destination.'pp.jpg';
			$profil->update_by = 1;
			$profil->save();
			return json_encode(array('success' => true, 'filename' => URL::to($destination.'pp.jpg')));
		}
		return json_encode(array('success' => false, 'msg' => 'Gagal ganti logo apotek'));
	}
	
}

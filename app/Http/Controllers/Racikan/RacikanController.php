<?php namespace App\Http\Controllers\Racikan;

use App\Racikan;
use App\Barang;
use App\Dokter;
use App\DetailRacikan;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RacikanController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$data['racikan'] = Racikan::where('status', true)->whereHas('dokter', function($query){
			$query->where('status', true)->whereHas('confHarga', function($q){
				$q->where('status', true);
			});
		})
		->get();
		$data['dokter'] = Dokter::where('status', true)->whereHas('confHarga', function($q){
			$q->where('status', true);
		})->get();
		$data['barang'] = Barang::where('status', true)->get();
		return view('setup/racikan', $data);
	}

	public function getAll()
	{
		$data['racikan'] = Racikan::where('status', true)->whereHas('dokter', function($query){
			$query->where('status', true)->whereHas('confHarga', function($q){
				$q->where('status', true);
			});
		})->get();
		return view('setup/racikanTable', $data);
	}

	public function getDataOne($id)
	{
		return Racikan::where('id', $id)->with(['detailRacikan' => function($q){
			$q->where('status', true);
		}, 'detailRacikan.barang' => function($q){
			$q->where('status', true);
		}])->first();
	}

	public function getKomposisi($id)
	{
		return Racikan::with('detailRacikan.barang')->with('dokter.confHarga')->find($id)->komposisi();
	}

	public function postDelete(Request $request)
	{
		$racikan = Racikan::find($request['id']);
		if($racikan->update(['update_by' => 1, 'status' => false]))
			return $racikan;
		else
			return false;
	}

	public function postDeleteDetail(Request $request)
	{
		$detail = DetailRacikan::find($request['id']);
		if($detail->update(['update_by' => 1, 'status' => false]))
			return $detail;
		else
			return false;
	}

	public function postUpdate(Request $request)
	{
		$racikan = Racikan::find($request['id']);
		$data = $request->all();
		if($racikan->update($data + ['update_by' => 1])){
			if($data["jenis"] == "Statis"){
				$total = sizeof($data['detail_barang_id']);
				for($i = 0; $i < $total; $i++){
					if($data['detail_id'][$i] == "" && $data['detail_barang_id'][$i] != ""){
						if(!DetailRacikan::create(array(
							'racikan_id' => $racikan->id, 
							'barang_id' => $data['detail_barang_id'][$i],
							'qty' => $data['detail_qty'][$i],
							'update_by' => 1))){
							return false;
						}
					}elseif($data['detail_id'][$i] != ""){
						if(!DetailRacikan::find($data['detail_id'][$i])->update([
							'qty' => $data['detail_qty'][$i],
							'update_by' => 1
							]))
						return false;
					}
				}
			}
			return $racikan;
		}else{
			return false;
		}
	}

	public function postUpdateDetail(Request $request)
	{
		$detail = DetailRacikan::find($request['id']);
		$detail->qty = $request['qty'];
		$detail->update_by = 1;
		if($detail->update())
			return $detail;
		else
			return false;
		
	}

	public function postInsert(Request $request)
	{
		if($racikan = Racikan::create($request->all() + ['update_by' => 1])){
			$detail = $request->only(['detail_barang_id','detail_qty']);
			$total = sizeof($detail['detail_barang_id']);
			for($i = 0; $i < $total; $i++){
				if($detail['detail_barang_id'][$i] != "" || $detail['detail_qty'][$i] != "")
					if(!DetailRacikan::create(array(
						'racikan_id' => $racikan->id, 
						'barang_id' => $detail['detail_barang_id'][$i],
						'qty' => $detail['detail_qty'][$i],
						'update_by' => 1)))
					return false;
			}
			return $racikan;
		}else{
			return false;
		}
	}

}

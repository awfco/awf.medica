<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\ProfilApotek;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!$request->session()->has('username'))
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect('auth/login');
			}
		}else{
			if (!$request->ajax())
			{
				$profile = ProfilApotek::first();
				if((!($profile) || $profile->nama == '') && $request->path() != 'setup/apotek')
					return redirect('setup/apotek')->with('msg', 'Harap isi data apotek terlebih dahulu');
			}
		}

		return $next($request);
	}

}

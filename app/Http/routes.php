<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controller('auth', 'Login\LoginController');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', function(){
		return redirect('/auth/login');
	});
	/* Penjualan */
	Route::controller('penjualan/transaksi', 'Penjualan\TransaksiController');
	
	Route::controller('penjualan/retur', 'Penjualan\ReturController');
	
	/* Pembelian */
	Route::get('pembelian/transaksi', function(){
		return view('pembelian/transaksi');
	});
	Route::get('pembelian/pelunasan', function(){
		return view('pembelian/pelunasan');
	});
	Route::get('pembelian/retur', function(){
		return view('pembelian/retur');
	});
	/* Setup */
	Route::controller('setup/barang', 'Barang\BarangController');
	
	Route::controller('setup/racikan', 'Racikan\RacikanController');
	
	Route::controller('setup/pasien', 'Pasien\PasienController');
	Route::controller('setup/apotek', 'Profil\ApotekController');
	
	Route::get('setup/opname', function(){
		return view('setup/opname');
	});
	
	Route::get('setup/supplier', function(){
		return view('setup/supplier');
	});
	
	Route::controller('setup/confharga', 'ConfHarga\ConfHargaController');
	
	Route::controller('setup/dokter', 'Dokter\DokterController');
	
	/* Laporan */
	Route::controller('laporan', 'Laporan\LaporanController');
	

});
/* Login */

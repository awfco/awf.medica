<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanDetailPenjualan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['no_nota', 'nama_barang', 'qty', 'harga_beli', 'harga_jual', 'diskon'];

}

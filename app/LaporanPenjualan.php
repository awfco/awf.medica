<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanPenjualan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['no_nota', 'tgl_penjualan', 'jenis', 'pasien', 'dokter', 'ppn', 'diskon', 'total_dokter', 'total_tunai', 'total_debit', 'bank_debit', 'dokter_lunas'];

}

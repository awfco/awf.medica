<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanReturPenjualan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['no_nota', 'nama_barang', 'qty', 'jumlah', 'update_by'];


}

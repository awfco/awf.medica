<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Neraca extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['kode_transaksi', 'deskripsi', 'total', 'tgl_transaksi', 'user_id'];

	public function user(){
        return $this->belongsTo('App\User');
	}
}

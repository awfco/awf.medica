<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'usia', 'notelp', 'alamat', 'dokter_id', 'ket', 'update_by', 'status'];

	public function dokter(){
        	return $this->belongsTo('App\Dokter');
	}
}

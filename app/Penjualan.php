<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model {
	
	protected $guarded = ['id'];
	
	protected $fillable = ['no_nota', 'tgl_penjualan', 'jenis', 'pasien_id', 'ppn', 'disc', 'status', 'update_by'];

	public function pasien(){
		return $this->belongsTo('App\Pasien');
	}

	public function detailPenjualan(){
		return $this->hasMany('App\DetailPenjualan');
	}
}

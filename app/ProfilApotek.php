<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilApotek extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'alamat', 'notelp', 'logo', 'footernota', 'update_by'];

}

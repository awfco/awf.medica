<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Racikan extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['nama', 'dokter_id', 'jenis','fee_kap', 'fee_dokter', 'jml_kapsul', 'update_by', 'status'];

	public function dokter(){
        return $this->belongsTo('App\Dokter')->where('dokters.status', true);
	}

	public function detailRacikan(){
		return $this->hasMany('App\DetailRacikan')->where('detail_racikans.status', true);
	}

	public function total(){
		$total = 0;
		foreach($this->detailRacikan as $detail){
			$total += $detail->harga();
		}
		$total = ($total / $this->jml_kapsul) + ($this->fee_kap + $this->fee_dokter);
		return $total;
	}

	public function komposisi(){
		$racikan = [];
		foreach($this->detailRacikan as $detail){
			$detail->qty = $detail->qty / $this->jml_kapsul;
			$detail->barang->harga = $detail->barang->harga + ($detail->barang->harga * $this->dokter->confHarga->margin);
			array_push($racikan, $detail);
		}
		return $racikan;
	}
}

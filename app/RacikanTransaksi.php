<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RacikanTransaksi extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['detail_penjualan_id', 'barang_id', 'racikan_id', 'total', 'created_at', 'update_at'];

}

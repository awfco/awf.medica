<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $guarded = ['id'];
	
	protected $fillable = ['username', 'password', 'role', 'status', 'update_by'];

}

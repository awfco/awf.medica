<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoktersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dokters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama');
			$table->string('spesialis');
			$table->string('alamat');
			$table->string('notelp');
			$table->integer('conf_harga_id')->unsigned();
			$table->boolean('status')->default(true);
			$table->integer('update_by')->unsigned();
			$table->foreign('update_by')->references('id')->on('users');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));

			$table->foreign('conf_harga_id')->references('id')->on('conf_hargas');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dokters');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenjualansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('penjualans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('tgl_penjualan');
			$table->string('jenis');
			$table->integer('pasien_id')->nullable()->unsigned();
			$table->float('ppn');
			$table->float('disc');
			$table->boolean('status')->default(true);
			$table->integer('update_by')->unsigned();
			$table->foreign('update_by')->references('id')->on('users');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));

			$table->foreign('pasien_id')->references('id')->on('pasiens');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('penjualans');
	}

}

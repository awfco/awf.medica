<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPenjualansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail_penjualans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('penjualan_id')->unsigned();
			$table->integer('barang_id')->nullable()->unsigned();
			$table->integer('racikan_id')->nullable()->unsigned();
			$table->float('total');
			$table->float('disc');
			$table->integer('update_by')->unsigned();
			$table->foreign('update_by')->references('id')->on('users');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));

			$table->foreign('penjualan_id')->references('id')->on('penjualans');
			$table->foreign('barang_id')->references('id')->on('barangs');
			$table->foreign('racikan_id')->references('id')->on('racikans');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_penjualans');
	}

}

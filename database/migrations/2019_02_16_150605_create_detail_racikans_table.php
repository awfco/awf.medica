<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailRacikansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail_racikans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('racikan_id')->unsigned();
			$table->integer('barang_id')->unsigned();
			$table->boolean('status')->default(true);
			$table->integer('update_by')->unsigned();
			$table->foreign('update_by')->references('id')->on('users');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail_racikans');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeRacikan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('racikans', function(Blueprint $table)
		{
			$table->float('fee_kap')->default(0);
			$table->float('fee_dokter')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('racikans', function(Blueprint $table)
		{
			$table->dropColumn('fee_kap');
			$table->dropColumn('fee_dokter');
		});
	}

}

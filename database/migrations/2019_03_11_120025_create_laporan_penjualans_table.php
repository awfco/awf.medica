<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanPenjualansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laporan_penjualans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('no_nota');
			$table->date('tgl_penjualan');
			$table->string('jenis');
			$table->string('dokter');
			$table->string('pasien');
			$table->float('ppn');
			$table->float('diskon');
			$table->bigInteger('total_dokter');
			$table->bigInteger('total_tunai');
			$table->bigInteger('total_debit');
			$table->string('bank_debit');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laporan_penjualans');
	}

}

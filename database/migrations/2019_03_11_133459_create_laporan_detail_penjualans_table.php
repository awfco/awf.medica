<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanDetailPenjualansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laporan_detail_penjualans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('no_nota');
			$table->string('nama_barang');
			$table->float('qty');
			$table->bigInteger('harga_beli');
			$table->bigInteger('harga_jual');
			$table->float('diskon');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laporan_detail_penjualans');
	}

}

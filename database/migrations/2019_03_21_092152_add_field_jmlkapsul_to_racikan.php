<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJmlkapsulToRacikan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('racikans', function(Blueprint $table)
		{
			$table->integer('jml_kapsul')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('racikans', function(Blueprint $table)
		{
			$table->dropColumn('jml_kapsul');
		});
	}

}

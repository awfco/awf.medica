<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacikanTransaksisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('racikan_transaksis', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('detail_penjualan_id')->unsigned();
			$table->integer('racikan_id')->unsigned();
			$table->integer('barang_id')->unsigned();
			$table->float('total');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('racikan_transaksis');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLaporanPenjualanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			$table->boolean('dokter_lunas')->after('bank_debit')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			$table->dropColumn('dokter_lunas');
		});
	}

}

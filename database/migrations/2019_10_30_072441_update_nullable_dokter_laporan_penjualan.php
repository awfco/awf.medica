<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNullableDokterLaporanPenjualan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			$table->string('dokter')->nullable()->change();
			$table->string('pasien')->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			//
		});
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIsretur extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			$table->boolean('is_retur')->after('dokter_lunas')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('laporan_penjualans', function(Blueprint $table)
		{
			$table->dropColumn('is_retur');
		});
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBarangIdLaporanDetailPenjualan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('laporan_detail_penjualans', function(Blueprint $table)
		{
			$table->integer('barang_id')->unsigned()->after('id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('laporan_detail_penjualans', function(Blueprint $table)
		{
			$table->dropColumn('barang_id');
		});
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFeeTableDokter extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dokters', function(Blueprint $table)
		{
			$table->bigInteger('fee')->after('alamat');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dokters', function(Blueprint $table)
		{
			$table->dropColumn('fee');
		});
	}

}

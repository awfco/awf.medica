<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdateByLaporanReturPenjualan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('laporan_retur_penjualans', function(Blueprint $table)
		{
			$table->integer('update_by')->unsigned()->after('jumlah');
			$table->foreign('update_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('laporan_retur_penjualans', function(Blueprint $table)
		{
			$table->dropForeign(['update_by']);
			$table->dropColumn('update_by');
		});
	}

}

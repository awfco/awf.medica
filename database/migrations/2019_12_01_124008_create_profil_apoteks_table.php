<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilApoteksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profil_apoteks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama')->nullable();
			$table->string('alamat')->nullable();
			$table->string('notelp')->nullable();
			$table->string('logo')->nullable();
			$table->string('footernota')->nullable();
			$table->integer('update_by')->unsigned();
			$table->foreign('update_by')->references('id')->on('users');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profil_apoteks');
	}

}

<?php

use App\User;
use App\Barang;
use App\ConfHarga;
use App\Dokter;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->call('BarangTableSeeder');
		$this->call('ConfHargaTableSeeder');
		$this->call('DokterTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('barangs')->delete();
        DB::table('conf_hargas')->delete();
        DB::table('dokters')->delete();
        DB::table('users')->delete();
		DB::statement("ALTER TABLE users AUTO_INCREMENT =  1");

        User::create(['username' => 'admin', 'password' => md5('admin'), 'role' => 'owner', 'update_by' => '1']);
    }

}

class BarangTableSeeder extends Seeder {

    public function run()
    {
        DB::table('barangs')->delete();
		DB::statement("ALTER TABLE barangs AUTO_INCREMENT =  1");

		Barang::create(['jenis' => 'Psikotropika', 'nama' => 'Amoxcilin', 'stock' => '15.0', 
		'harga' => '500', 'tanggal_kadaluarsa' => '2020-01-12', 'satuan' => 'mg',
		'update_by' => '1']);

		Barang::create(['jenis' => 'Psikotropika', 'nama' => 'Neozep', 'stock' => '100.0', 
		'harga' => '200', 'tanggal_kadaluarsa' => '2020-01-12', 'satuan' => 'mg',
		'update_by' => '1']);
    }

}

class ConfHargaTableSeeder extends Seeder {

    public function run()
    {
        DB::table('conf_hargas')->delete();
		DB::statement("ALTER TABLE conf_hargas AUTO_INCREMENT =  1");

		ConfHarga::create(['nama' => 'Harga Dr. Haidar', 'margin' => '0.3',
		'update_by' => '1']);
    }

}

class DokterTableSeeder extends Seeder {

    public function run()
    {
        DB::table('dokters')->delete();
		DB::statement("ALTER TABLE dokters AUTO_INCREMENT =  1");

		Dokter::create(['nama' => 'Dr. Haidar Alatas, SpPD-KGH, M.H., M.M.', 'spesialis' => 'Penyakit Dalam', 'alamat' => 'Jl. Ahmad Yani no.26', 
		'notelp' => '0281640795', 'conf_harga_id' => '1', 'update_by' => '1']);
    }

}

function convertToRupiah(angka)
{
	if(parseInt(angka) == 0 || angka == null || isNaN(angka)) return "Rp. 0";
	angka = Math.round(angka)
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}
/**
 * Usage example:
 * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
 */
 
function convertToAngka(rupiah)
{
	return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}

$("input[type='number']").on('focus', function(e){
	if($(e.target).val() == 0){
		$(e.target).val("")
	}
})
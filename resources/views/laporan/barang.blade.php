@extends('index')
@section('judulpage')
    Laporan Barang
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
@endsection
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cart-outline m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Laporan</li>
                        <li class="breadcrumb-item active">Laporan Barang</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Laporan Barang</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>No. Nota</th>
                                    <th>Barang</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($laporans as $laporan)
                                <tr>
                                    <td class="isijal">{{$count}}</td>
                                    <td class="isijal">{{$laporan->no_nota}}</td>
                                    <td class="isijal">{{ucfirst($laporan->nama_barang)}}</td>
                                    <td class="isijal">{{$laporan->qty}}</td>
                                </tr>
                            <?php $count += 1 ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
    });
</script> 
@endsection
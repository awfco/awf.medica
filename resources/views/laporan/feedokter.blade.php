@extends('index')
@section('judulpage')
    Laporan Fee Dokter
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cart-outline m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Laporan</li>
                        <li class="breadcrumb-item active">Laporan Fee Dokter</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Laporan Fee Dokter</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Transaksi</th>
                                    <th>Tgl. Transaksi</th>
                                    <th>Dokter</th>
                                    <th>Fee</th>
                                    <th>Pelunasan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $count = 1; 
                                    $totalFee = 0;
                                    $totalLunas = 0;
                                ?>
                                @foreach($laporans as $laporan)
                                <tr>
                                    <td class="isijal">{{$count}}</td>
                                    <td class="isijal">{{$laporan->no_nota}}</td>
                                    <td class="isijal">{{substr($laporan->tgl_penjualan, 0, 10)}}</td>
                                    <td class="isijal">{{$laporan->dokter}}</td>
                                    <td class="isijal">{{"Rp. ".number_format($laporan->total_dokter,0,',','.')}}</td>
                                    <td class="isijal">{{"Rp. ".number_format($laporan->dokter_lunas,0,',','.')}}</td>
                                </tr>
                                    <?php 
                                        $count += 1;
                                        $totalFee += $laporan->total_dokter;
                                        $totalLunas += $laporan->dokter_lunas;
                                    ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <td colspan="4" align="right">Total :</td>
                                <td align="center">{{"Rp. ".number_format($totalFee,0,',','.')}}</td>
                                <td align="center">{{"Rp. ".number_format($totalLunas,0,',','.')}}</td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(function() {
        $(".select2").select2();
    });
    $(function() {
        $('#myTable').DataTable();
    });
    $('#sa-warning').click(function(){
        swal({   
            title: "Anda yakin ingin menghapus data ini?",   
            text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Hapus",   
            closeOnConfirm: false 
        }, function(){   
            swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
        });
    });
</script> 
@endsection
@extends('index')
@section('judulpage')
    Laporan Laba Rugi
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
@endsection
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cart-outline m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Laporan</li>
                        <li class="breadcrumb-item active">Laporan Laba Rugi</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Laporan Laba Rugi</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>No Nota</th>
                                    <th>Keterangan</th>
                                    <th>Harga Jual</th>
                                    <th>Harga Beli</th>
                                    <th>Laba</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $count = 1; 
                                    $totalIncome = 0;
                                    $totalOutcome = 0;
                                ?>
                                @foreach($laporans as $laporan)
                                <?php 
                                    $laba = ($laporan->harga_jual * $laporan->qty) - ($laporan->harga_beli * $laporan->qty);
                                    $labatext = ($laba < 0) ? "(Rp.".number_format(($laba * -1), 0, ',', '.').")" : "Rp.".number_format($laba, 0, ',', '.');
                                ?>
                                <tr>
                                    <td class="isijal">{{$count}}</td>
                                    <td class="isijal">{{$laporan->no_nota}}</td>
                                    <td class="isijal">{{$laporan->nama_barang}}</td>
                                    <td class="isijal">{{"Rp. ".number_format(($laporan->harga_jual * $laporan->qty),0,',','.')}}</td>
                                    <td class="isijal">{{"Rp. ".number_format(($laporan->harga_beli * $laporan->qty),0,',','.')}}</td>
                                    <td class="isijal" <?php if($laba < 0){ ?> style="color:red" <?php } ?>>{{$labatext}}</td>
                                </tr>
                                <?php 
                                    $count += 1;
                                    $totalIncome += ($laporan->harga_jual * $laporan->qty);
                                    $totalOutcome += ($laporan->harga_beli * $laporan->qty);
                                ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <?php
                                    $totallaba = $totalIncome - $totalOutcome;
                                    $totallabatext = ($totallaba < 0) ? "(Rp.".number_format(($totallaba * -1), 0, ',', '.').")" : "Rp.".number_format($totallaba, 0, ',', '.');
                                ?>
                                <td colspan="3" align="right">Total :</td>
                                <td align="center">{{"Rp. ".number_format($totalIncome,0,',','.')}}</td>
                                <td align="center">{{"Rp. ".number_format($totalOutcome,0,',','.')}}</td>
                                <td align="center" <?php if($totallaba < 0){ ?> style="color:red" <?php } ?>><b>{{$totallabatext}}</b></td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
    });
</script> 
@endsection
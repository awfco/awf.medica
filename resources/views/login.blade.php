<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 14:36:20 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/awf tok-04.png') }}">
    <title>AwfMedica</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- page css -->
    <link href="{{ asset('css/pages/login-register-lock.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/awfstyle.css') }}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ asset('css/colors/default-dark.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div style="position:absolute; top: calc(50% - 40px); left: calc(50% - 40px)">
            <img src="{{ asset('/img/loader.gif') }}"/><br/>
            <span style="
                font-size: 0.875em;
                letter-spacing: 0.1em;
                line-height: 1.5em;
                color: #1976d2;
                white-space: nowrap;">AwfMedica</span>
        </div>
        <!-- <div class="loader"> -->
            <!-- <div class="loader__figure"></div> -->
            <!-- <p class="loader__label">Awfmedica</p> -->
        <!-- </div> -->
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url({{ asset('/img/bgjunctionuhui.png') }});">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform">
                    <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('/img/awf tok-04.png') }}" alt="Home" height="40px" width="40px"/><br/><img src="{{ asset('/img/awf medica-05.png') }}" alt="Home" height="31px" width="108px"/></a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input name="username" class="form-control" type="text" required="" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input name="password" class="form-control form-control-line" type="password" required="" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <span class="text-danger salahpass"><small><i class="fa fa-exclamation-triangle"></i> Username atau Password salah</small></span>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn bglogin btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('/js/popper.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".salahpass").hide();
            $(".preloader").fadeOut();
        });
        $("#loginform").on('submit', function(){
            console.log('wowowow')
            $.post("{{URL::to('auth/proses')}}", $(this).serialize())
            .done(function(result){
                res = JSON.parse(result)
                if(res){
                    window.location.href = "{{URL::to('penjualan/transaksi')}}"
                }else{
                    $(".salahpass").show();
                }
            });
            return false;
        })
    </script>
    
</body>


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 14:36:20 GMT -->
</html>
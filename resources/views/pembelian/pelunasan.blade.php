@extends('index')
@section('judulpage')
    Pelunasan Faktur
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
@endsection
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-briefcase-check m-b-0 warnabirumudaicon"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Pembelian</li>
                        <li class="breadcrumb-item active">Pelunasan Faktur</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnabirumudabg"></span>Pelunasan Faktur</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>No. Faktur</th>
                                    <th>Tanggal Beli</th>
                                    <th>Tanggal Jatuh Tempo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="isijal">1</td>
                                    <td class="isijal">00121</td>
                                    <td class="isijal">21-12-2018</td>
                                    <td class="isijal">21-12-2018</td>
                                    <td class="isijal">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Bayar</button>
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="exampleModalLabel1">Retur Pembelian</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">No. Faktur</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md"><p class="form-control-static">No. Faktur</p></div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">Tgl. Faktur</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md"><p class="form-control-static">Tgl. Faktur</p></div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">Supplier</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md"><p class="form-control-static">Nama Supplier</p></div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">Jumlah</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md">
                                                                        <input type="text" class="form-control form-control-sm" placeholder="Jumlah">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">Cara Pembayaran</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md">
                                                                        <select class="custom-select custom-select-sm" name="jenis">
                                                                            <option value="">Tunai</option>
                                                                            <option value="">Debit</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-5"><p class="form-control-static">Keterangan</p></div>
                                                                    <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                    <div class="col-md">
                                                                        <textarea class="form-control form-control-sm" placeholder="Keterangan"></textarea>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="tst3 btn-sm btn-success" data-dismiss="modal">Confirm</button>
                                                            <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
    });
    $(function() {
      $(".tst3").click(function(){
           $.toast({
            heading: ' Success Message ',
            text: 'Pelunasan Faktur Berhasil Disimpan',
            position: 'top-right',
            loaderBg:'#ffffff',
            icon: 'success'
          });
     });
    });
    $('#myTable').DataTable( {
        "columnDefs": [ {
          "targets": [3],
          "orderable": false,
        }]
        });
</script> 
@endsection
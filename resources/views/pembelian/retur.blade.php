@extends('index')
@section('judulpage')
    Retur Pembelian
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-briefcase-upload m-b-0 warnamerah"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Pembelian</li>
                        <li class="breadcrumb-item active">Retur Pembelian</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnamerahbg"></span>Retur Pembelian</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>No. Faktur</th>
                                    <th>Tanggal Beli</th>
                                    <th>Tanggal Expired Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="isijal">1</td>
                                    <td class="isijal">00121</td>
                                    <td class="isijal">21-12-2018</td>
                                    <td class="isijal">21-12-2018</td>
                                    <td align="center"><button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Retur</button>
                                            <div class="modal fade long-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="exampleModalLabel1">Retur Pembelian</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                        <form>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-4"><p class="form-control-static">Sebab</p></div>
                                                                            <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                            <div class="col-md"><textarea class="form-control" placeholder="Sebab Retur"></textarea></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-5"><p class="form-control-static">Nama Supplier</p></div>
                                                                            <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                            <div class="col-md"><p class="form-control-static">Nama Supplier</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-4"><p class="form-control-static">No. Faktur</p></div>
                                                                            <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                            <div class="col-md"><p class="form-control-static">No. Faktur</p></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-5"><p class="form-control-static">Tgl. Beli</p></div>
                                                                            <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                            <div class="col-md"><p class="form-control-static">Tgl. Beli</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered" style="font-size: small">
                                                                        <thead>
                                                                            <tr align="center" class="putih">
                                                                                <th>
                                                                                    <input type="checkbox" id="all" class="chk-col-teal"/>
                                                                                    <label for="all"></label>
                                                                                </th> 
                                                                                <th class="tengah">Kode</th>
                                                                                <th>Nama</th>
                                                                                <th>Qty</th>
                                                                                <th>Expired Date</th>
                                                                                <th>Harga Satuan</th>
                                                                                <th>Gross</th>
                                                                                <th>Disc.</th>
                                                                                <th>Sub Total</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr align="center">
                                                                                <td>
                                                                                    <input type="checkbox" id="satusatu" class="chk-col-teal" checked/>
                                                                                    <label for="satusatu"></label>
                                                                                </td>
                                                                                <td>KD001</td>
                                                                                <td>Amoxcillin</td>
                                                                                <td width="13%"><input type="number" value="0" class="form-control form-control-sm" id="example-number-input" style="font-size: small" min="0"></td>
                                                                                <td>12/21/2021</td>
                                                                                <td>Rp. 40.000,-</td>
                                                                                <td>Rp. 40.000,-</td>
                                                                                <td>-</td>
                                                                                <td>Rp. 40.000,-</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-4"><p class="form-control-static"></p></div>
                                                                            <div class="col-md-1"><p class="form-control-static"></p></div>
                                                                            <div class="col-md"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group row">
                                                                            <div class="col-md-5"><p class="form-control-static">Total</p></div>
                                                                            <div class="col-md-1"><p class="form-control-static">:</p></div>
                                                                            <div class="col-md"><p class="form-control-static">Total</p></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="tst3 btn-sm btn-success" data-dismiss="modal">Retur</button>
                                                            <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
        $('#tabelretur').DataTable();
    });
    $('#myTable').DataTable( {
        "columnDefs": [ {
          "targets": [3],
          "orderable": false,
        }]
        });
    $(function() {
      $(".tst3").click(function(){
           $.toast({
            heading: ' Success Message ',
            text: 'Retur Pembelian Berhasil Disimpan',
            position: 'top-right',
            loaderBg:'#ffffff',
            icon: 'success'
          });
     });
    });
</script> 
@endsection
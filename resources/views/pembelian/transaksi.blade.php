@extends('index')
@section('judulpage')
    Transaksi Pembelian
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->           
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-briefcase-download m-b-0 warnabirumudaicon"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Pembelian</li>
                        <li class="breadcrumb-item active">Transaksi Pembelian</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnabirumudabg"></span>Transaksi Pembelian</h3>
                    <br/>
                    <form>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">No. Faktur</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                    <input type="text" class="form-control form-control-sm" placeholder="No.Faktur" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-3"><label class="control-label ">Kode Supplier</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-xs-4">
                                    <div class="input-group">
                                    <input type="text" class="form-control form-control-sm" placeholder="Kode Supplier" name="">
                                        <div class="input-group-append">
                                            <button class="btn-sm btn-success" type="button" data-toggle="modal" data-target="#ModalCariSupplier" data-whatever="@mdo"><i class="mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                 <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Tgl. Transaksi</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md"><p class="form-control-static">Tgl. Transaksi</p></div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-3"><label class="control-label ">Nama Supplier</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-xs-4"><p class="form-control-static">Nama Supplier</p></div>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                 <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Tgl. Jatuh Tempo</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                    <input type="date" class="form-control form-control-sm" placeholder="dd/mm/yyyy" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-3"><label class="control-label ">Jenis Supplier</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-xs-4"><p class="form-control-static">Jenis Supplier</p></div>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                 <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Nama Sales</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md"><input type="text" class="form-control form-control-sm" placeholder="Nama Sales" name=""></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="font-size: small" >
                                    <thead>
                                        <tr align="center"> 
                                            <th>Kode</th>
                                            <th>Nama</th>
                                            <th>Satuan</th>
                                            <th>Jenis</th>
                                            <th>Expired Date</th>
                                            <th>Harga</th>
                                            <th>Qty</th>
                                            <th>Disc. %</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr align="center">
                                            <td>KD001</td>
                                            <td>Amoxcillin</td>
                                            <td>Strip</td>
                                            <td>OOT</td>
                                            <td>10</td>
                                            <td>Rp. 8.000,-</td>
                                            <td>5</td>
                                            <td>-</td>
                                            <td>Rp. 40.000,-</td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" class="btn-sm btn-success"><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn-sm btn-success sa-warning "><i class="fas fa-trash-alt"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td width="10%">
                                                <input type="text" class="form-control form-control-sm" placeholder="Kode" name=""style="font-size: small">
                                            </td>
                                            <td width="12%">
                                                <input type="text" class="form-control form-control-sm" placeholder="Nama Brg" style="font-size: small">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control form-control-sm" placeholder="Satuan" name=""style="font-size: small">
                                            </td>
                                            <td width="23%">
                                            <select class="custom-select custom-select-sm" name="jenis">
                                                <option value="HV">HV</option>
                                                <option value="Ethical">Ethical</option>
                                                <option value="Prekusor">Prekusor</option>
                                                <option value="OOT">OOT</option>
                                                <option value="Psikotropika">Psikotropika</option>
                                                <option value="Narkotika">Narkotika</option>
                                                <option value="Alkes">Alkes</option>
                                                <option value="Lain-lain">Lain-lain</option>
                                            </select>
                                            </td>
                                            <td width="8%">
                                                <input type="date" class="form-control form-control-sm" placeholder="dd/mm/yyyy" style="font-size: small" name="">
                                            </td>
                                            <td width="8%"> 
                                                <input type="number" value="0" class="form-control form-control-sm" id="example-number-input" style="font-size: small" min="0">
                                            </td>
                                            <td width="8%">
                                                <input type="number" value="0" class="form-control form-control-sm" id="example-number-input" style="font-size: small" min="0">
                                            </td>
                                            <td width="8%">
                                                <input type="number" value="0" class="form-control form-control-sm" id="example-number-input" style="font-size: small" min="0" >
                                            </td>
                                            <td></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" class="btn-sm btn-success"><i class="mdi mdi-plus"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Sub Total</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <p class="form-control-static"> Sub Total </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">PPN</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <input type="number" class="form-control form-control-sm" placeholder="PPN" value="0.1" min="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Diskon</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <input type="text" class="form-control form-control-sm" placeholder="Diskon">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Grand Total</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <p class="form-control-static"> Grand Total </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                            <div class="col-md-.">
                                <button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#ModalBayar" data-whatever="@mdo"><i class="fa fa-check"></i> Confirm</button>
                                <button type="button" class="btn-sm btn-inverse"><i class="fa ti-reload"></i> Reset</button>
                                <!--Modal Bayar-->
                                <div class="modal fade" id="ModalBayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1" align="center">Validasi</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" style="font-size: small">
                                                            <thead>
                                                                <tr align="center"> 
                                                                    <th>Nama</th>
                                                                    <th>Qty</th>
                                                                    <th>Jumlah</th>
                                                                </tr>
                                                            </thead>
                                                             <tbody>
                                                                <tr align="center">
                                                                    <td>Amoxcillin</td>
                                                                    <td>5</td>
                                                                    <td>Rp. 40.000,-</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><label class="control-label ">Grand Total</label></div>
                                                        <div class="col-md-1"><label class="control-label ">:</label></div>
                                                        <div class="col-md"><p class="form-control-static">Grand Total</p></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><label class="control-label ">Pembayaran Tunai</label></div>
                                                        <div class="col-md-1"><label class="control-label ">:</label></div>
                                                        <div class="col-md">
                                                        <input type="text" class="form-control form-control-sm" placeholder="Nominal">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><label class="control-label ">Pembayaran Debit / Kredit</label></div>
                                                        <div class="col-md-1"><label class="control-label ">:</label></div>
                                                        <div class="col-md">
                                                            <input type="text" class="debit form-control form-control-sm" placeholder="Nominal">&nbsp;ATM&nbsp;
                                                            <input type="text" class="atm form-control form-control-sm" placeholder="ATM">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-4"><label class="control-label ">Kekurangan</label></div>
                                                        <div class="col-md-1"><label class="control-label ">:</label></div>
                                                        <div class="col-md"><p class="form-control-static">Kekurangan</p></div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button id="ShowBtn" type="button" class="btn-sm btn-success sa-su-sayang">OK</button>
                                                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Modal Bayar-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
<div class="modal fade" id="ModalCariSupplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1" align="center">Data Supplier</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive m-t-40">
                    <table id="TabelSupplier" class="table table-bordered table-hover">
                        <thead align="center">
                            <tr>
                                <th>Kode</th>
                                <th>Nama Supplier</th>
                                <th>Nama Sales</th>
                                <th>Jenis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="isijal">TK-121-ASD	</td>
                                <td class="isijal">Kimia Farma</td>
                                <td class="isijal">Baldum</td>
                                <td class="isijal">PBF</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script>
$(document).ready(function() {
    var table = $('#TabelSupplier').DataTable();
 
    $('#TabelSupplier tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    //$('#button').click( function () {
        //table.row('.selected').remove().draw( false );
    //} );
    $(".select2").select2();
    $(".tst3").click(function(){
           $.toast({
            heading: ' Success Message ',
            text: 'Transaksi Pembelian Berhasil Disimpan',
            position: 'top-right',
            loaderBg:'#ffffff',
            icon: 'success'
          });
    });
    $('.sa-warning').click(function(){
        swal({   
            title: "Anda yakin ingin menghapus barang ini?",   
            text: "",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Hapus",   
            closeOnConfirm: false  
        });
    });
    $(document).on('click', '.SwalBtn1', function() {
        console.log('Button 1');
        swal.clickConfirm();
    });
    $(document).on('click', '.SwalBtn2', function() {
        console.log('Button 2');
        swal.clickConfirm();
    });
    $('.sa-su-sayang').click(function(){
        swal({
            type: 'question',
            title: 'Ingin cetak berapa nota?',
            html:  
            '<button type="button" role="button" tabindex="0" class="SwalBtn1 btn btn-success">' + '1 Nota' + '</button> &nbsp;' + 
            '<button type="button" role="button" tabindex="0" class="SwalBtn2 btn btn-success">' + '2 Nota' + '</button>',
            showCancelButton: false,
            showConfirmButton: false
        });
    });
});
</script>
@endsection
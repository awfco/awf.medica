@extends('index')
@section('judulpage')
    Retur Penjualan
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
@endsection
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cart-off m-b-0 warnamerah"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Penjualan</li>
                        <li class="breadcrumb-item active">Retur Penjualan</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnamerahbg"></span>Retur Penjualan</h3>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>No. Nota</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Penjualan</th>
                                    <th>Pasien</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($penjualans as $key => $penjualan)
                                    <tr>
                                        <td>{{($key + 1)}}</td>
                                        <td>{{$penjualan->no_nota}}</td>
                                        <td>{{date_format(date_create(substr($penjualan->tgl_penjualan, 0, 10)),"d - M - Y")}}</td>
                                        <td>{{$penjualan->jenis}}</td>
                                        <td>{{($penjualan->pasien_id) ? $penjualan->pasien->nama : "-"}}</td>
                                        <td align="center">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn-sm btn-success btnModal" data-toggle="modal" data-target="#modalRetur" data-id="{{$penjualan->id}}">Retur</button>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalRetur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="isimodal">
                        @include('penjualan/returModal')
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
        $(".btnModal").on("click", function(){
            $.get("{{URL::to('/penjualan/retur/openmodal')}}/"+$(this).data("id"))
            .done(function(res){
                $("#isimodal").html(res)
            })
        })
    });
</script> 
@endsection
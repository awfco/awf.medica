
<div class="modal-header">
    <h4 class="modal-title" id="exampleModalLabel1">Retur Penjualan</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
    <form id="formRetur">
        <input type="hidden" name="id" value='{{($data != "") ? $data->id : ""}}'/>
        <div class="form-group row">
            <div class="col-md-5"><p class="form-control-static">Jenis Transaksi</p></div>
            <div class="col-md-1"><p class="form-control-static">:</p></div>
            <div class="col-md"><p class="form-control-static">{{($data != "") ? $data->jenis : ""}}</p></div>
        </div>
        <div class="form-group row">
            <div class="col-md-5"><p class="form-control-static">Dokter</p></div>
            <div class="col-md-1"><p class="form-control-static">:</p></div>
            <div class="col-md"><p class="form-control-static">{{($data != "") ? (($data->pasien) ? $data->pasien->dokter->nama : "") : ""}}</p></div>
        </div>
        <div class="form-group row">
            <div class="col-md-5"><p class="form-control-static">Pasien</p></div>
            <div class="col-md-1"><p class="form-control-static">:</p></div>
            <div class="col-md"><p class="form-control-static">{{($data != "") ? (($data->pasien) ? $data->pasien->nama : "") : ""}}</p></div>
        </div>
        <div class="form-group row">
            <div class="col-md-5"><p class="form-control-static">Tanggal Transaksi</p></div>
            <div class="col-md-1"><p class="form-control-static">:</p></div>
            <div class="col-md"><p class="form-control-static">{{($data != "") ? date_format(date_create(substr($data->tgl_penjualan, 0, 10)),"d - M - Y") : ""}}</p></div>
        </div>
        <div class="form-group row">
            <div class="col-md-5"><p class="form-control-static">List Item</p></div>
            <div class="col-md-1"><p class="form-control-static">:</p></div>
        </div>
        <div class="form-group row">
            <table id="tableModal" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="20px"><input type="checkbox" class="form-control" id="checkAll"/><label for="checkAll">&nbsp;</label></th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data != "")
                        @foreach($data->detailPenjualan as $listItem)
                            <tr>
                                <td>
                                    @if($listItem->barang_id && $listItem->total > 0)
                                        <input class="form-control checkitem" type="checkbox" id="check{{$listItem->id}}" value="{{$listItem->id}}" name="barang_id[]"/><label for="check{{$listItem->id}}">&nbsp;</label>
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                                <td>{{($listItem->barang_id) ? $listItem->barang->nama : $listItem->racikan->nama}}</td>
                                <td>
                                    @if($listItem->barang_id && $listItem->total > 0)
                                        <span class="total{{$listItem->id}}">{{$listItem->total}}</span>
                                        <input data-hb = "{{$listItem->laporanDetailPenjualan()->harga_beli}}" data-hj = "{{$listItem->laporanDetailPenjualan()->harga_jual}}" class="form-control totalInput totalInput{{$listItem->id}}" type="number" max="{{$listItem->total}}" value="{{$listItem->total}}" name="qty_barang[{{$listItem->id}}]" min="0" style="display:none"/>
                                    @else
                                        {{$listItem->total}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <input class="form-control" id="radioretur0" type="radio" value="0" name="returtype" checked/><label for="radioretur0">Retur Full</label><br/>
                <span class="hargajual">Rp. 0</span>
            </div>
            <div class="col-md-6">
                <input class="form-control" id="radioretur1" type="radio" value="1" name="returtype"/><label for="radioretur1">Retur HPP</label><br/>
                <span class="hargabeli">Rp. 0</span>
            </div>
        </div>
        <input type="hidden" name="totalretur" id="totalretur"/>
    </form>
</div>
<div class="modal-footer">
    <button type="submit" class="tst3 btn-sm btn-success" id="btnSubmit" disabled>Retur</button>
    <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
</div>
<script>
    var totalhb = 0.0;
    var totalhj = 0.0;
    if($('#tableModal').length > 0){
        $('#tableModal').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false
        });
        $("#tableModal_wrapper").attr("style", "width:95%;margin:0 auto")
    }
    var lastfocus = 0
    $(".totalInput").on("focus", function(){
        lastfocus = parseFloat($(this).val())
    })
    $(".totalInput").on("change", function(){
        totalhb -= parseFloat($(this).data("hb") * lastfocus)
        totalhb += parseFloat($(this).data("hb") * $(this).val())
        totalhj -= parseFloat($(this).data("hj") * lastfocus)
        totalhj += parseFloat($(this).data("hj") * $(this).val())
        $(".hargajual").html(convertToRupiah(totalhj))
        $(".hargabeli").html(convertToRupiah(totalhb))
        lastfocus = parseFloat($(this).val())
    })
    $(".checkitem").on("click", function(){
        var id = $(this).val()
        if($(this).is(":checked")){
            totalhb += parseFloat($(".totalInput"+id).data("hb") * $(".totalInput"+id).val())
            totalhj += parseFloat($(".totalInput"+id).data("hj") * $(".totalInput"+id).val())
            $("#btnSubmit").removeAttr("disabled")
            $(".totalInput"+id).show()
            $(".total"+id).hide()
            if($(".checkitem").length == $(".checkitem:checked").length){
                $("#checkAll").trigger("click")
            }
        }else{
            totalhb -= parseFloat($(".totalInput"+id).data("hb") * $(".totalInput"+id).val())
            totalhj -= parseFloat($(".totalInput"+id).data("hj") * $(".totalInput"+id).val())
            $(".total"+id).show()
            $(".totalInput"+id).hide()
            if($(".checkitem").length != $(".checkitem:not(:checked)").length){
                $("#checkAll").trigger("click")
            }
            if($(".checkitem:checked").length == 0){
                $("#btnSubmit").attr("disabled", true)
            }
        }
        $(".hargajual").html(convertToRupiah(totalhj))
        $(".hargabeli").html(convertToRupiah(totalhb))
        if($("#radioretur0").is(":checked"))
            $("#totalretur").val(totalhj)
        else
            $("#totalretur").val(totalhb)
    })
    $(".radioretur0").on("click", function(){
        $("#totalretur").val(totalhj)
    })
    $(".radioretur1").on("click", function(){
        $("#totalretur").val(totalhb)
    })
    $("#checkAll").on("click", function(){
        if($("#checkAll").is(":checked")){
            $(".checkitem:not(:checked)").trigger("click")
        }else{
            if($(".checkitem").length == $(".checkitem:checked").length){
                $(".checkitem:checked").trigger("click")
            }
        }
    })
    $("#btnSubmit").on("click", function(e){
        e.preventDefault()
        $.post("{{URL::to('/penjualan/retur/returbarang')}}", $("#formRetur").serialize())
        .done(function(result){
            $("#modalRetur").modal('hide');
            $.toast({
                heading: ' Success Message ',
                text: 'Retur penjualan berhasil',
                position: 'top-right',
                loaderBg:'#ffffff',
                icon: 'success'
            });
        })
    })
</script>
@extends('index')
@section('judulpage')
    Transaksi Penjualan
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->           
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cart-outline m-b-0 warnahijaumuda"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Penjualan</li>
                        <li class="breadcrumb-item active">Transaksi Penjualan</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnahijaumudabg"></span>Transaksi Penjualan </h3>
                    <br/>
                    <form id = "formTransaksi">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">No Nota</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <input type="hidden" name="nonota" value="{{ $nonota }}"/>
                                    <div class="col-md"><p class="form-control-static">{{ $nonota }}</p></div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Jenis Transaksi</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <div class="m-b-10">
                                            <label class="custom-control custom-radio">
                                                <input id="jenisnr" value="Non-Resep" name="jenis" type="radio" class="custom-control-input" checked/>
                                                <span class="custom-control-label">Non-Resep</span>
                                            </label>
                                            <label class="custom-control custom-radio">
                                                <input id="jenisr" value="Resep" name="jenis" type="radio" class="custom-control-input">
                                                <span class="custom-control-label">Resep</span>
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Pasien</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md selectPasien">
                                        <select class="select2" style="width:100%;" id="Pasien" name="pasien_id" <?php echo ($pasien == [])?"disabled":""; ?>>
                                            <option value="">Select</option> 
                                            @foreach($pasien as $data)
                                            <?php $dkt = $data->dokter->nama; $dkt_id = $data->dokter_id; ?>
                                            <option value="{{$data->id}}">{{$data->nama}}</option>
                                            @endforeach
                                            <option id="newPasien" value="NewPasien">Input Data Baru</option>
                                        </select>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Tanggal</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <input type="hidden" name="tglTransaksi" value="{{ $today }}"/>
                                        <p class="form-control-static"> {{ $today }} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Dokter</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <select class="select2" style="width:100%" id="Dokter" name="dokter_id" disabled>
                                            <option value="">Pilih Dokter</option>
                                            @foreach($dokter as $data)
                                            <option value="{{$data->id}}">{{$data->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Alamat</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <p class="form-control-static" id="alamatPasien"> </p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="table-responsive tableDetailTransaksi">
                                <table class="table table-bordered" style="font-size: small">
                                    <thead>
                                        <tr align="center"> 
                                            <th>Kode</th>
                                            <th>Nama</th>
                                            <th>Satuan</th>
                                            <th>Jenis</th>
                                            <th>Stok</th>
                                            <th>Harga</th>
                                            <th>Qty</th>
                                            <th>Disc. %</th>
                                            <th>Disc. Rp</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listObat">
                                        <tr align="center" class="row0">
                                            <input type="hidden" name="barang[]" class="obatId0" value=""/>
                                            <input type="hidden" name="qty[]" class="obatQty0" value=""/>
                                            <input type="hidden" name="type[]" class="obatType0" value=""/>
                                            <input type="hidden" name="discOne[]" class="discOne0" value="0"/>
                                            <td class="kodeObat0"></td>
                                            <td width="15%">
                                                <div class="input-group namaObat0">
                                                    <input type="text" class="form-control form-control-sm" placeholder="Cari" style="font-size: small">
                                                        <div class="input-group-append">
                                                            <button class="btn-sm btn-success" type="button" data-toggle="modal" data-target="#ModalCariBarang" data-whatever="@mdo"><i class="mdi mdi-magnify"></i></button>
                                                        </div>
                                                </div>
                                            </td>
                                            <td class="satuanObat0"></td>
                                            <td class="jenisObat0"></td>
                                            <td class="stokObat0"></td>
                                            <td class="hargaObat0"></td>
                                            <td width="10%" class="qtyObat0">
                                                <input class="form-control form-control-sm qty" type="number" value="0" id="example-number-input" style="font-size: small" data-row = "0">
                                            </td>
                                            <td width="10%" class="discObat0">
                                                <input class="form-control form-control-sm disc" type="number" id="example-number-input" style="font-size: small" placeholder="Disc.">
                                            </td>
                                            <td class="discRpObat0"></td>
                                            <td class="totalObat0"></td>
                                            <td class="actionObat0">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button type="button" class="btn-sm btn-success btnAddRow"><i class="mdi mdi-plus"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">`Sub Total`</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <p class="form-control-static subTotal"> 0 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">PPN</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <input type="number" name="ppn" class="form-control form-control-sm ppn" value = "0" min = "0" step="0.1" placeholder="PPN">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Diskon</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <input type="number" name="disc" class="form-control form-control-sm grandDisc"  value = "0" min = "0" step="0.1" placeholder="Diskon">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md"></div>
                            <div class="col-md">
                                <div class="form-group row">
                                    <div class="col-md-5"><label class="control-label ">Grand Total</label></div>
                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                    <div class="col-md">
                                        <p class="form-control-static gt"> 0 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                            <div class="col-md-.">
                                <button type="button" class="btn-sm btn-success btnBayar" data-toggle="modal" data-target="#ModalBayar" data-whatever="@mdo"><i class="fa fa-check"></i> Bayar</button>
                                <button type="button" class="btn-sm btn-inverse btnReset"><i class="fa ti-reload"></i> Reset</button>
                                <!--Modal Bayar-->
                                <div class="modal fade" id="ModalBayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1" align="center">Validasi</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="font-size: small">
                                                        <thead>
                                                            <tr align="center"> 
                                                                <th>Nama</th>
                                                                <th>Qty</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                            <tbody class="itemBayar">
                                                            <tr align="center">
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Grand Total</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md"><p class="form-control-static gt">Grand Total</p></div>
                                                    <input type="hidden" class="formGT" name="grandtotal" value=0/>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Pembayaran Tunai</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                    <input type="number" class="form-control form-control-sm tunaiForm" placeholder="Nominal" value=0 name="total_tunai">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Pembayaran Debit / Kredit</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="number" class="debitForm form-control form-control-sm" placeholder="Nominal" style="width:150px" value=0 name="total_debit">&nbsp;
                                                        <select class="custom-select custom-select-sm" style="width:100px" name="bank_debit">
                                                            <option value="">BCA</option>
                                                            <option value="">BNI</option>
                                                            <option value="">Mandiri</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Kembalian</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md"><p class="form-control-static kembalian">0</p></div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Total Print</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input value="0" name="print" type="radio" class="custom-control-input" checked/>
                                                                <span class="custom-control-label">Tidak Print</span>
                                                            </label>
                                                            <label class="custom-control custom-radio">
                                                                <input value="1" name="print" type="radio" class="custom-control-input"/>
                                                                <span class="custom-control-label">1</span>
                                                            </label>
                                                            <label class="custom-control custom-radio">
                                                                <input value="2" name="print" type="radio" class="custom-control-input"/>
                                                                <span class="custom-control-label">2</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="tst3 btn-sm btn-success btnConfirm" data-dismiss="modal">Confirm</button>
                                                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Modal Bayar-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>

    
<!--Modal Cari Barang-->
<div class="modal fade" id="ModalCariBarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1" align="center">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive m-t-40 listBarang">
                    @include('penjualan.transaksiBarang')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--End Modal Cari Barang-->
<!-- Modal Pasien -->

<div class="modal fade" id="ModalTambahPasien" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Tambah Data Pasien</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="formPasien">
                <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">Nama Pasien</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <input type="text" class="form-control form-control-sm" placeholder="Nama Pasien" name="nama">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">Usia</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <input type="text" class="form-control form-control-sm" placeholder="Usia" name="usia">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">No. Telp</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <input type="text" class="form-control form-control-sm" placeholder="No. Telp" name="notelp">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">Alamat</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <input type="text" class="form-control form-control-sm" placeholder="Alamat" name="alamat">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">Dokter</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <input type="hidden" value="{{$dkt_id}}" name="dokter_id"/>
                                <span> {{$dkt}} </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4"><label class="control-label ">Keterangan</label></div>
                            <div class="col-md-1"><label class="control-label ">:</label></div>
                            <div class="col-md">
                                <textarea class="form-control" placeholder="Keterangan" name="ket"></textarea>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submimt" class="tst3 btn-sm btn-success">Tambah</button>
                    <button type="button" class="btn-sm btn-inverse btncancel" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal pasien -->
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/jquery.hotkeys.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/printThis.js')}}"></script>
<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script>
var listItem = {}
$(document).ready(function() {
    var alamat = {
        @foreach($pasien as $data)
        {{$data->id}}: "{{$data->alamat}}",
        @endforeach
    }
    $(document).bind('keydown', 'f5', function(e){
        e.preventDefault()
        $('#ModalCariBarang').modal('show')
    })
    $(document).bind('keydown', 'f1', function(e){
        e.preventDefault()
        $('.btnBayar').trigger('click')
    })
    $("#Pasien").on('change', function(){
        if($(this).val() == ""){
            $("#alamatPasien").html("")
        }else if($(this).val() == "NewPasien"){
            $("#alamatPasien").html("")
            $("#ModalTambahPasien").modal("show")
        }else{
            if(($(this).val() in alamat)){
                $("#alamatPasien").html(alamat[$(this).val()])
            }else{
                $("#alamatPasien").html("")
            }
        }
    })
    function resetForm(){
        $('#formPasien').trigger('reset');
    }
    $("#formPasien").on('submit', function(e){
        e.preventDefault()
        var tipe = "simpan"
        var url = "{{URL::to('/setup/pasien/insert')}}"
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: url,
            data: $("#formPasien").serialize(),
            success: function(data){
                if(data != false){
                    resetForm();
                    $('.selectPasien').load("{{URL::to('/penjualan/transaksi/pasien')}}/"+data.dokter_id, function(){
                        $("#Pasien").val(data.id)
                        $("#Pasien").select2()
                        $("#alamatPasien").html(data.alamat)
                    })
                    $('#ModalTambahPasien').modal("hide")
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        })
    });
    $('#ModalBayar').on('shown.bs.modal', function () {
        $('.tunaiForm').trigger('focus')
    })
    $('#ModalTambahPasien').on('hidden.bs.modal', function () {
        resetForm();
        if($("#Pasien").val() == "NewPasien"){
            $("#Pasien").val("")
            $("#Pasien").select2()
        }
    })
    var table = $('#TabelBarang').DataTable({
        destroy: true,
        select: {
            style: 'single'
        },
        info: false
    });
    $('.print').hide()
    $('.btnConfirm').on('click', function(){
        if(parseInt($(".debitForm").val()) + parseInt($(".tunaiForm").val()) < parseInt($(".formGT").val())){
            swal("Uang bayar kurang", "Total pembayaran kurang!", "error"); 
        }else{
            temp_form = $("#hiddenLater").html()
            $("#hiddenLater").html("")
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "post",
                url: "{{URL::to('/penjualan/transaksi/bayar')}}",
                data: $("#formTransaksi").serialize(),
                success: function(data){
                    window.location.reload(true)
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
            $("#hiddenLater").html(temp_form)
            // swal({
            //     type: 'question',
            //     title: 'Ingin cetak berapa nota?',
            //     html:  
            //     '<button type="button" role="button" tabindex="0" class="SwalBtn1 btn btn-success">' + '1 Nota' + '</button> &nbsp;' + 
            //     '<button type="button" role="button" tabindex="0" class="SwalBtn2 btn btn-success">' + '2 Nota' + '</button>',
            //     showCancelButton: false,
            //     showConfirmButton: false
            // });
            // $('.print').printThis({
            //     importCSS: true,
            //     importStyle: true,
            //     beforePrint: function(){
            //         $(".print").show()
            //     },
            //     afterPrint: function(){
            //         $(".print").hide()
            //     }
            // }) 
        }
    })
    $(".select2").select2();
    var currRow = 0;
    var currRacikan = 0;
    var state = ""

    function emptyRow(count){ 
        rowName = (state == "") ? "row" : "racikanActive"
        countInside = (state == "") ? String(count) : String(count) + String(currRacikan)
        return `
        <tr align="center" class="${rowName}${count}">
            <input type="hidden" name="barang[]" class="obatId${state}${countInside}" value=""/>
            <input type="hidden" name="qty[]" class="obatQty${state}${countInside}" value=""/>
            <input type="hidden" name="type[]" class="obatType${state}${countInside}" value=""/>
                <input type="hidden" name="discOne[]" class="discOne${state}${countInside}" value="0"/>
            ${(state == 'Dinamis') ? `
                <input type="hidden" name="qtyOne[]" class="qtyOne${countInside}" value="1" />
            ` : ""}
            <td ${(state == "") ? `class="kodeObat${countInside}"` : ""}></td>
            <td width="15%">
                <div class="input-group namaObat${state}${countInside}">
                    <input type="text" class="form-control form-control-sm" placeholder="Cari" style="font-size: small">
                        <div class="input-group-append">
                            <button class="btn-sm btn-success" type="button" data-toggle="modal" data-target="#ModalCariBarang" data-whatever="@mdo"><i class="mdi mdi-magnify"></i></button>
                        </div>
                </div>
            </td>
            <td class="satuanObat${state}${countInside}"></td>
            <td class="jenisObat${state}${countInside}"></td>
            <td class="stokObat${state}${countInside}"></td>
            <td class="hargaObat${state}${countInside}"></td>
            <td width="10%" class="qtyObat${state}${countInside}">
                <input class="form-control form-control-sm qty${state}" type="number" value="0" id="example-number-input" style="font-size: small" data-row = "${count}">
            </td>
            ${(state == "") ? `
            <td width="10%" class="discObat${countInside}">
                <input class="form-control form-control-sm disc" type="number" id="example-number-input" style="font-size: small" placeholder="Disc."}>
            </td>
            <td class="discRpObat${countInside}"></td>
            <td class="totalObat${countInside}"></td>
            ` : `
            <td></td>
            <td></td>
            <td></td>
            `}
            <td class="actionObat${state}${countInside}">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn-sm btn-success btnAddRow"><i class="mdi mdi-plus"></i></button>
                </div>
            </td>
        </tr>
    `}

    function actionDinamis(row){
        return `
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn-sm btn-success btnAccept" data-id="${row}"><i class="mdi mdi-check"></i></button>
                <button type="button" class="btn-sm btn-success btnCancel" data-id="${row}"><i class="mdi mdi-close"></i></button>
            </div>
        `
    }

    function racikanRow(racikanNo, detail){ 
        var data = ""
        detail.forEach(function(item, index){
            data += `
                <tr align="center" class="racikan${racikanNo}">
                    <input type="hidden" name="barang[]" value="${item.barang.id}"/>
                    <input type="hidden" name="qtyOne[]" class="qtyOne${racikanNo}${index}" value="${item.qty}" />
                    <input type="hidden" name="qty[]" class="obatQty${state}${racikanNo}${index}" value="${item.qty}" />
                    <input type="hidden" name="type[]" value="nonRacikanRacikan" />
                    <input type="hidden" name="discOne[]" class="discOne${state}${racikanNo}${index}" value="0"/>
                    <td></td>
                    <td width="15%">
                        ${item.barang.nama}
                    </td>
                    <td>${item.barang.satuan}</td>
                    <td>${item.barang.jenis}</td>
                    <td>${item.barang.stock}</td>
                    <td class="hargaRacikan${racikanNo}${index}">${item.barang.harga}</td>
                    <td class="qtyRacikan${racikanNo}${index}">
                        ${item.qty}
                    </td>
                    <td width="10%">
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
        `})
        $("#listObat").append(data)
    }

    function addNewRow(cRow){
        qtyrow = $(".qtyObat"+cRow+" input").val()
        discrow = $(".discObat"+cRow+" input").val()
        $(".namaObat"+state+cRow).html( $(".namaObat"+state+cRow+" input").val() )
        $(".qtyObat"+state+cRow).html($(".qtyObat"+state+cRow+" input").val())
        $(".discObat"+state+cRow).html($(".discObat"+state+cRow+" input").val())
        $(".obatQty"+state+cRow).val(qtyrow)
        $(".discOne"+state+cRow).val(discrow)
        $(".actionObat"+state+cRow).html(`
            <button type="button" class="btn-sm btn-success editBtnObat non${state}" data-id="${(state=="") ? cRow : currRow}" data-racikan="${(state=="Dinamis") ? cRow : currRacikan}"><i class="mdi mdi-pencil"></i></button>
            <button id="sa-warning" type="button" class="btn-sm btn-success deleteBtnObat non${state}" data-id="${(state=="") ? cRow : currRow}" data-racikan="${(state=="Dinamis") ? cRow : currRacikan}"><i class="fas fa-trash-alt"></i></button>
        `)
        if(state == ""){
            currRow += 1
            subtotal = 0
            for(row = 0; row < currRow; row++){
                subtotal += parseInt($('.totalObat'+row).html())
            }
            $('.subTotal').html(subtotal)
            $('.ppn').trigger('keyup')
        }else{
            $(".racikanActive"+currRow).addClass("racikan"+currRow)
            $(".racikanActive"+currRow).addClass("racikanDinamis"+currRacikan)
            $(".racikan"+currRow).removeClass("racikanActive"+currRow)
            currRacikan += 1
        }
        $("#listObat").append(emptyRow(currRow))
    }

    function toggleState(newState){
        state = newState
        if(state == "Dinamis"){
            $(".non").hide()
        }else{
            $(".racikanActive"+currRow).remove()
            currRacikan = 0
            $(".nonDinamis").remove()
            $(".non").show()
            $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang')}}/"+$("#Dokter").val()+"/0", function(){
                table = $('#TabelBarang').DataTable({
                    destroy: true,
                    select: {
                        style: 'single'
                    },
                    info: false
                });
                tableFunctions()
            })
        }
    }

    function editRow(count){
        count = (state == "") ? String(count) : String(currRow) + String(count)
        namaList = $('.namaObat'+state+count).html() + $('.kodeObat'+state+count).html()
        oldQty = $('.qtyObat'+state+count).html()
        listItem[namaList] = listItem[namaList] - parseInt(oldQty)
        $('.qtyObat'+state+count).html(`
                <input class="form-control form-control-sm qty${state}" type="number" value="${oldQty}" id="example-number-input" style="font-size: small" data-row = "${count}" data-list="${namaList}">
        `)
        if(state == ""){
            $('.row'+currRow).remove()
            oldDisc = $('.discObat'+state+count).html()
            $('.discObat'+state+count).html(`
                    <input class="form-control form-control-sm disc" type="number" id="example-number-input" value="${oldDisc}" style="font-size: small" placeholder="Disc.">
            `)
            currRow-- 
        }else{
            $('.racikanActive'+currRow).remove()
            currRacikan--
        }
        $('.actionObat'+state+count).html(`
            <button type="button" class="btn-sm btn-success btnAddRow" data-id="${count}"><i class="mdi mdi-plus"></i></button>
        `)
    }

    function newRow(){
        if(typeof $('.btnAddRow').data('id') !== 'undefined'){
            cRow = (state == "") ? String($('.btnAddRow').data('id')) : String(currRow)+String($('.btnAddRow').data('id'))
        }else{
            cRow = (state == "") ? String(currRow) : String(currRow) + String(currRacikan)
        }
        if($('.obatId'+state+cRow).val() == ""){
            swal("Data tidak lengkap", "Harap isi obat terlebih dahulu sebelum menambahkan baris", "error"); 
        }else if($(".qty"+state+cRow).val() <= 0){
            swal("Data tidak lengkap", "Harap isi qty > 0", "error"); 
        }else if(parseInt($(".qty"+state+cRow).val()) > listItem[$(".qty"+state+cRow).data("list")]){
            swal("Out of Stock", "Jumlah barang yang dibeli melebihi stock")
        }else{
            listItem[$(".qty"+state+cRow).data("list")] = listItem[$(".qty"+state+cRow).data("list")] - parseInt($(".qty"+state+cRow).val())
            addNewRow(cRow)
            $(".namaObat"+cRow+" button").focus()
        }
    }

    function tableFunctions(){
        $('#ModalCariBarang').on('shown.bs.modal', function(){
            $('div.dataTables_filter input', table.table().container()).focus();
            table.row({ filter : 'applied'}, 0).select()
        })
        $('#TabelBarang tbody').on( 'click', 'tr', function () {
            if($(this).hasClass('selected')){
                selectedRow()
            }
        });
        $('div.dataTables_filter input', table.table().container()).keyup( function (e) {
            if (e.keyCode == 13) {
                selectedRow()
            }else{
                table.row({ filter : 'applied'}, 0).select()
            }
        } );
    }
    tableFunctions()

    

    $('#listObat').on('click', '.btnAddRow', newRow)
    
    $("#listObat").on('click', '.editBtnObat', function(){
        editRow($(this).data('id'))
    })

    $("#listObat").on('click', '.btnCancel, .deleteBtnObat', function(){
        count = (state == "") ? String($(this).data('id')) : String(currRow) + String($(this).data('id'))
        namaList = $('.namaObat'+state+count).html() + $('.kodeObat'+state+count).html()
        oldQty = $('.qtyObat'+state+count).html()
        if(!($(this).hasClass('btnCancel')) && state == "Dinamis"){
            $('.racikan'+$(this).data('id')+".racikanDinamis"+$(this).data('racikan')).remove();
        }else{
            $('.row'+$(this).data('id')).remove();
            $('.racikan'+$(this).data('id')).remove();
            $('.racikanActive'+$(this).data('id')).remove();
            if($(this).hasClass('btnCancel')){
                toggleState("")
                $('#listObat').append(emptyRow($(this).data('id')))
            }
        }
    });

    $("#listObat").on('click', '.btnAccept', function(){
        cRow = $(this).data('id')
        hargaSatuan = parseInt($('.hargaObat'+cRow).html())
        console.log(hargaSatuan)
        jmlKapsul = $(`.qtyObat${cRow} input`).val()
        total = hargaSatuan * jmlKapsul
        for(row = 0; row < currRacikan; row++){
            rowz = String(currRow) + String(row)
            jmlSatuan = (parseFloat($(".qtyObatDinamis"+rowz).html()) / jmlKapsul)
            hargaSatuan += $(".hargaObatDinamis"+rowz).html() * jmlSatuan
            $(`.qtyOne${rowz}`).val(jmlSatuan)
            total += $(".hargaObatDinamis"+rowz).html() * $(".qtyObatDinamis"+rowz).html()
            $(`.namaObatDinamis${rowz}`).removeClass('input-group')
        }
        diskon = hargaSatuan * $(`.discObat${cRow} input`).val()
        $('.hargaObat'+cRow).html(hargaSatuan)
        $(`.discRpObat${cRow}`).html(diskon)
        $(`.totalObat${cRow}`).html(total - diskon)
        toggleState("")
        addNewRow(cRow)
        $(".namaObat"+cRow+" button").focus()
    })

    function selectedRow(){
        dataRow = table.row({ selected: true }).data()
        rowActive = (state == "") ? currRow : currRacikan
        for(row = 0; row < rowActive; row++){
            rowz = (state == "") ? row : String(currRow) + String(row)
            if((dataRow[2].includes("Racikan") && $('.jenisObat'+state+rowz).html().includes("Racikan")) ||
                (!(dataRow[2].includes("Racikan")) && !($('.jenisObat'+state+rowz).html().includes("Racikan")))){
                if($('.obatId'+state+rowz).val() == dataRow[0]){
                    editRow(row)
                    $('.qtyObat'+state+rowz+' input').data("max", dataRow[4])
                    $('.qtyObat'+state+rowz+' input').focus()
                    $('#ModalCariBarang').modal('hide')
                    $(".qty").trigger('focus')
                    table.search("").draw()
                    return;
                }
            }
        }

        if(state == "") $('.racikan'+rowActive).remove()
        else rowActive = String(currRow) + String(rowActive)
        $('div.dataTables_filter input').val('')
        console.log(dataRow[2].includes("Racikan"))
        if(dataRow[2].includes("Racikan"))
            $('.obatType'+state+rowActive).val("racikan")
        else{
            if(state == "Dinamis")
                $('.obatType'+state+rowActive).val("nonRacikanRacikan")
            else
                $('.obatType'+state+rowActive).val("nonRacikan")
        }
        listItem[dataRow[1]+dataRow[0]] = listItem[dataRow[2]+dataRow[0]] || parseInt(dataRow[4])
        $('.kodeObat'+state+rowActive).html(dataRow[0])
        $('.satuanObat'+state+rowActive).html(dataRow[3])
        $('.jenisObat'+state+rowActive).html(dataRow[2])
        $('.stokObat'+state+rowActive).html(dataRow[4])
        $('.hargaObat'+state+rowActive).html(dataRow[5])
        $(".namaObat"+state+rowActive+" input").val(dataRow[1])
        $(".qty"+state).val("1")
        $(".qty"+state).data("max", dataRow[4])
        $(".qty"+state).data("list", dataRow[2]+dataRow[0])
        $('.qtyOne'+state+rowActive).val("1")
        $('.obatId'+state+rowActive).val(dataRow[0])
        if(state == ""){
            $(".disc"+state).val("0")
            $(".qty").trigger("keyup")
        }
        $('#ModalCariBarang').modal('hide')
        $(".qty").trigger('focus')
        table.search("").draw()
        if(dataRow[2] == 'Racikan|Statis'){
            $.ajax({
                method: "get",
                url: "{{URL::to('setup/racikan/komposisi')}}/" + dataRow[0],
                success: function(data){
                    racikanRow(currRow, data)
                }
            })
        }else if(dataRow[2] == 'Racikan|Dinamis'){
            toggleState("Dinamis")
            $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang')}}/"+$("#Dokter").val()+"/1", function(){
                table = $('#TabelBarang').DataTable({
                    destroy: true,
                    select: {
                        style: 'single'
                    },
                    info: false
                });
                tableFunctions()
            })
            $('.actionObat'+rowActive).html(actionDinamis(rowActive))
            $(".namaObat"+rowActive).html( $(".namaObat"+rowActive+" input").val() )
            $("#listObat").append(emptyRow(rowActive))
        }
    }

    function changeTotal(row, isRacikan){
        // console.log($('.disc').val())
        console.log($('.qty').val() * $('.hargaObat'+row).html())
        $(".discRpObat"+row).html($('.disc').val() * $('.hargaObat'+row).html())
        total = (parseInt($(".hargaObat"+row).html()) - parseInt($(".discRpObat"+row).html())) * $(".qty").val()
        thisState = state
        if(isRacikan){
            if(thisState == ""){
                if($(".jenisObat"+row).html().indexOf("Dinamis")!== -1){
                    thisState = "Dinamis"
                }
            }
            $('.racikan'+row).each(function(i, obj){
                lama = parseFloat($('.qtyOne'+String(row)+String(i)).val())
                $('.qtyRacikan'+String(row)+String(i)).html(lama * $(".qty").val())
                $('.qtyObatDinamis'+String(row)+String(i)).html(lama * $(".qty").val())
                $('.obatQty'+thisState+String(row)+String(i)).val(lama * $(".qty").val())
            })
        }
        $(".totalObat"+row).html(total)
    }

    function resetTable(id){
        $("#listObat").html("")
        currRow = 0
        toggleState("")
        $('.subTotal').html('0')
        $('.ppn').val(0)
        $('.gt').html('0')
        $('.formGT').val(0)
        $('.diskon').val(0)
        $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang')}}/"+id+"/0", function(){
                table = $('#TabelBarang').DataTable({
                    destroy: true,
                    select: {
                        style: 'single'
                    },
                    info: false
                });
                tableFunctions()
            })
        $("#listObat").append(emptyRow(currRow))
    }

    $("#listObat").on('keyup change', '.qty',  function(e){
        row = $(this).data("row")
        var max = ($(this).data("max")) ? parseFloat($(this).data("max")) : 0
        isRacikan = $(".jenisObat"+row).html().indexOf("Racikan") !== -1
        if (e.keyCode === 13) {
            $(".disc").trigger('focus')
            changeTotal(row, isRacikan)
            return
        }
        if(parseFloat($(this).val()) > max){
            swal("Stock kurang", "Quantity melebihi jumlah stok!", "error"); 
            $(this).val($(this).data("max"))
        }
        changeTotal(row, isRacikan)
    })

    $('#listObat').on('keyup change', '.disc', function(e){
        $(".qty").trigger("change")
        if (e.keyCode === 13) {
            $(".btnAddRow").trigger('click');
        }
    })

    
    $("#jenisnr").on('click', function(){
        $("#Dokter").val("")
        $('#Dokter').select2()
        $("#Dokter").attr("disabled", true)
        $("#Pasien").val("")
        $('#Pasien').select2()
        $("#Pasien").attr("disabled", true)
        $("#alamatPasien").html("")
        $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang/0/0')}}", function(){
            table = $('#TabelBarang').DataTable({
                destroy: true,
                select: {
                    style: 'single'
                },
                info: false
            });
            tableFunctions()
        })
        resetTable(0)
        $('.tableDetailTransaksi').show()
    })

    $("#jenisr").on('click', function(){
        $("#Dokter").attr("disabled", false)
        $("#Pasien").attr("disabled", true)
        $('.tableDetailTransaksi').hide()
        resetTable(0)
    })

    $("#Dokter").on('change', function(){
        $("#alamatPasien").html("")
        if($(this).val() == ""){
            $("#Pasien").val("")
            $('#Pasien').select2()
            $("#Pasien").attr("disabled", "true")
            $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang/0/0')}}", function(){
                table = $('#TabelBarang').DataTable({
                    destroy: true,
                    select: {
                        style: 'single'
                    },
                    info: false
                });
                tableFunctions()
            })
        }else{
            $('.listBarang').load("{{URL::to('/penjualan/transaksi/barang')}}/"+$(this).val()+"/0", function(){
                table = $('#TabelBarang').DataTable({
                    destroy: true,
                    select: {
                        style: 'single'
                    },
                    info: false
                });
                tableFunctions()
            })
            $("#Pasien").attr("disabled", "false")
            $('.selectPasien').load("{{URL::to('/penjualan/transaksi/pasien')}}/"+$(this).val(), function(){
                $('.select2').select2();
                $("#Pasien").on('change', function(){
                    if($(this).val() != ""){
                        resetTable($("#Dokter").val())
                        $('.tableDetailTransaksi').show()
                    }else{
                        $('.tableDetailTransaksi').hide()
                    }
                })

            })
        }
    })

    $('.ppn, .grandDisc').on('keyup change', function(){
        subtotal = parseInt($('.subTotal').html())
        ppn = parseFloat($('.ppn').val())
        ppnRp = subtotal * ppn
        discount = parseFloat($('.grandDisc').val())
        discRp = subtotal * discount
        $('.gt').html( convertToRupiah(subtotal + ppnRp - discRp) )
        $('.formGT').val(subtotal + ppnRp - discRp)
        $('.ppnPersenPrint').html(`PPN (${ppn * 100}%)`)
        $('.discPersenPrint').html(`Discount (${discount * 100}%)`)
        $('.ppnPrint').html(convertToRupiah(ppnRp))
        $('.discPrint').html(convertToRupiah(discRp))
    })

    $('.tunaiForm, .debitForm').on('keyup change', function(){
        gt = $(".formGT").val()
        tunai = parseInt($('.tunaiForm').val())
        debit = parseInt($('.debitForm').val())
        kembalian = (tunai + debit) - gt
        $('.bayar').html(convertToRupiah(tunai + debit))
        // $('.tunai').html(convertToRupiah(tunai))
        $('.kembalian').html(convertToRupiah(kembalian))
    })

    $(".btnBayar").on('click', function(){
        $('.itemBayar').html('')
        for(row = 0; row < currRow; row++){
            $('.itemBayar').append(`
            <tr>
                <td>${$('.namaObat'+row).html()}</td>
                <td>${$('.qtyObat'+row).html()}</td>
                <td align="right">${convertToRupiah($('.totalObat'+row).html())}</td>
            </tr>
            `)
        }
    })

    $("#Pasien").on('change', function(){
        if($(this).val() != ""){
            resetTable($("#Dokter").val())
            $('.tableDetailTransaksi').show()
        }else{
            $('.tableDetailTransaksi').hide()
        }
        
    })

    $('.btnReset').on('click', function(){
        window.location.reload(true)
    })


    $('.sa-su-sayang').click(function(){
        swal({
            type: 'question',
            title: 'Cetak Nota',
            text: 'Anda ingin mencetak berapa nota?',
            showConfirmButton: false,
            footer: `<div class="row">
                        <div class="col-sm">
                            <button class="btn btn-success">1 Nota</button>&nbsp;
                            <button class="btn btn-success">2 Nota</button>
                        </div>
                    </div>`
        })
    });
 
    //$('#button').click( function () {
        //table.row('.selected').remove().draw( false );
    //} );
} );
</script>
@endsection
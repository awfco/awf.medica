<table id="TabelBarang" class="table table-bordered table-striped">
    <thead align="center">
        <tr>
            <th>Kode</th>
            <th>Nama</th>
            <th>Jenis</th>
            <th>Satuan</th>
            <th>Stok</th>
            <th>Harga</th>
        </tr>
    </thead>
    <tbody>
        @foreach($barang as $datBar)
        <tr>
            <td> {{$datBar->id}} </td>
            <td> {{ $datBar->nama }} </td>
            <td> {{ $datBar->jenis }} </td>
            <td> {{ $datBar->satuan }} </td>
            <td> {{ $datBar->stock }} </td>
            <td> {{ $datBar->total($picked_dokter) }} </td>
        </tr>
        @endforeach
        @foreach($racikan as $datRac)
        <tr class="listRacikan">
            <td> {{$datRac->id}} </td>
            <td> {{ $datRac->nama }} </td>
            <td> Racikan|{{ $datRac->jenis }} </td>
            <td> - </td>
            <td> - </td>
            <td> {{ $datRac->total() }} </td>
        </tr>
        @endforeach
    </tbody>
</table>
<select class="select2" style="width:100%;" id="Pasien" name="pasien_id" <?php echo ($pasien == [])?"disabled":""; ?>>
    <option value="">Select</option> 
    @foreach($pasien as $data)
    <?php $dkt = $data->dokter->nama; $dkt_id = $data->dokter_id; ?>
    <option value="{{$data->id}}">{{$data->nama}}</option>
    @endforeach
    <option id="newPasien" value="NewPasien">Input Data Baru</option>
</select>
<div id="hiddenLater">
    <div class="modal fade" id="ModalTambahPasien" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Tambah Data Pasien</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form id="formPasien">
                    <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">Nama Pasien</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <input type="text" class="form-control form-control-sm" placeholder="Nama Pasien" name="nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">Usia</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <input type="text" class="form-control form-control-sm" placeholder="Usia" name="usia">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">No. Telp</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <input type="text" class="form-control form-control-sm" placeholder="No. Telp" name="notelp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">Alamat</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <input type="text" class="form-control form-control-sm" placeholder="Alamat" name="alamat">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">Dokter</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <input type="hidden" value="{{$dkt_id}}" name="dokter_id"/>
                                    <span> {{$dkt}} </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4"><label class="control-label ">Keterangan</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md">
                                    <textarea class="form-control" placeholder="Keterangan" name="ket"></textarea>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submimt" class="tst3 btn-sm btn-success">Tambah</button>
                        <button type="button" class="btn-sm btn-inverse btncancel" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        var alamat = {
            @foreach($pasien as $data)
            {{$data->id}}: "{{$data->alamat}}",
            @endforeach
        }
        $("#Pasien").on('change', function(){
            if($(this).val() == ""){
                $("#alamatPasien").html("")
            }else if($(this).val() == "NewPasien"){
                $("#alamatPasien").html("")
                $("#ModalTambahPasien").modal("show")
            }else{
                if(($(this).val() in alamat)){
                    $("#alamatPasien").html(alamat[$(this).val()])
                }else{
                    $("#alamatPasien").html("")
                }
            }
        })
        function resetForm(){
            $('#formPasien').trigger('reset');
        }
        $("#formPasien").on('submit', function(e){
            e.preventDefault()
            var tipe = "simpan"
            var url = "{{URL::to('/setup/pasien/insert')}}"
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: url,
                data: $("#formPasien").serialize(),
                success: function(data){
                    if(data != false){
                        resetForm();
                        $('.selectPasien').load("{{URL::to('/penjualan/transaksi/pasien')}}/"+data.dokter_id, function(){
                            $("#Pasien").val(data.id)
                            $("#Pasien").select2()
                            $("#alamatPasien").html(data.alamat)
                        })
                        $('#ModalTambahPasien').modal("hide")
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
        $('#ModalTambahPasien').on('hidden.bs.modal', function () {
            resetForm();
            if($("#Pasien").val() == "NewPasien"){
                $("#Pasien").val("")
                $("#Pasien").select2()
            }
        })
    </script>
</div>
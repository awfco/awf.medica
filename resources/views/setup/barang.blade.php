@extends('index')
@section('judulpage')
    Data Barang
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-cube m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Data Barang</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Data Barang</h3>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                        <div class="col-md-2" align="right"><button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#ModalTambah" data-whatever="@mdo"><i class="mdi mdi-plus"></i> Tambah </button></div>
                            <!--Modal Tambah-->
                            <div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <form id="formBarang">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLabel1">Tambah Data Barang</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                                <input type="hidden" name="id" id=""/>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Nama Barang</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="namaBarang form-control form-control-sm" placeholder="Nama Barang" name="nama" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Jenis</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <select class="custom-select custom-select-sm" name="jenis">
                                                            <option value="">Pilih Jenis</option>
                                                            <option value="HV">HV</option>
                                                            <option value="Ethical">Ethical</option>
                                                            <option value="Prekusor">Prekusor</option>
                                                            <option value="OOT">OOT</option>
                                                            <option value="Psikotropika">Psikotropika</option>
                                                            <option value="Narkotika">Narkotika</option>
                                                            <option value="Alkes">Alkes</option>
                                                            <option value="Lain-lain">Lain-lain</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Satuan</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="satuanBarang form-control form-control-sm" placeholder="Satuan" name="satuan" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Stok</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="number" class="stokBarang form-control form-control-sm" placeholder="Stok" name="stock" min="0" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Expired Date</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                    <input type="date" class="edBarang form-control form-control-sm" placeholder="dd/mm/yyyy" name="tanggal_kadaluarsa" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Harga</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="hargaBarang form-control form-control-sm" placeholder="Harga" name="harga" min="0" required>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="tst3 btn-sm btn-success">Tambah</button>
                                            <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!--End Modal Tambah-->
                    </div>
                    <div class="table-responsive m-t-40">
                        @include('setup.barangTable')
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/jquery.alphanum.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable( {
          "columnDefs": [ {
          "targets": [7],
          "orderable": false,
        }]
        });
        $(".namaBarang").alphanum({
            allowOtherCharSets: false
        });
        $(".satuanBarang").alpha();
        $(".stokBarang").alphanum({
            allow: '.'
        });
        $(".hargaBarang").numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false
        });
        $('#myTable').DataTable();
        function resetForm(){
            $(".tst3").html("Tambah");
            $('#exampleModalLabel1').html('Tambah Barang');
            $('#formBarang').trigger('reset');
        }
        $('#ModalTambah').on('hidden.bs.modal', function () {
            resetForm();
        })
        $('.table-responsive').on('click', '.editBtn', function(e){
            var id = $(this).data('id');
            console.log(id)
            $.ajax({
                method: "get",
                url: "{{URL::to('/setup/barang/data-one')}}/"+id,
                success: function(data){
                    $(".tst3").html("Update");
                    $("#formBarang").populateForm(data);
                    $('#exampleModalLabel1').html('Edit Barang');
                    $("#ModalTambah").modal('show');
                }
            })
        });
        $('.table-responsive').on('click', '.deleteBtn', function(e){
            var id = $(this).data('id');
            console.log(id)
            swal({
                title: "Anda yakin ingin menghapus data ini?",   
                text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Hapus"
            }).then((willDelete) => {
                if(willDelete["value"]){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: "post",
                        url: "{{URL::to('/setup/barang/delete')}}",
                        data: "id="+id,
                        success: function(data){
                            if(data){
                                $('.table-response').fadeOut();
                                $('.table-responsive').load("{{URL::to('/setup/barang/all')}}",function(){
                                    $('.table-response').fadeIn();
                                    $('#myTable').DataTable();
                                });
                                swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
                            }else{
                                swal("Gagal!", "Data Gagal Dihapus.", "error"); 
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    })
                }
            });
        });
        $("#formBarang").on('submit', function(e){
            e.preventDefault()
            if($('.tst3').html() == "Tambah"){
                var tipe = "simpan"
                var url = "{{URL::to('/setup/barang/insert')}}"
            }else{
                var tipe = "update"
                var url = "{{URL::to('/setup/barang/update')}}"
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: url,
                data: $("#formBarang").serialize(),
                success: function(data){
                    if(data != false){
                        resetForm();
                        $("#ModalTambah").modal('hide');
                        $('.table-response').fadeOut();
                        $('.table-responsive').load("{{URL::to('/setup/barang/all')}}",function(){
                            $('.table-response').fadeIn();
                            $('#myTable').DataTable();
                        });
                        $.toast({
                            heading: ' Success Message ',
                            text: 'Data Barang Berhasil Di'+tipe,
                            position: 'top-right',
                            loaderBg:'#ffffff',
                            icon: 'success'
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
    });
</script> 
@endsection
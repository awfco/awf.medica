<table id="myTable" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th>No</th>
            <th width="20%">Nama</th>
            <th>Jenis</th>
            <th width="5%">Satuan</th>
            <th>Stok</th>
            <th>Expired Date</th>
            <th>Harga</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $i = 1;
            foreach($barang as $data){
        ?>
            <tr>
                <td class="isijal">{{ $i }}</td>
                <td class="isijal">{{ $data->nama }}</td>
                <td class="isijal">{{ $data->jenis }}</td>
                <td class="isijal">{{ $data->satuan }}</td>
                <td class="isijal">{{ $data->stock }}</td>
                <td class="isijal">{{ $data->tanggal_kadaluarsa }}</td>
                <td class="isijal">{{ "Rp. " . number_format($data->harga,0,',','.') }}</td>
                <td class="isijal">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn-sm btn-success editBtn" data-id = "{{ $data->id }}"><i class="mdi mdi-pencil"></i></button>
                        <button id="sa-warning" type="button" class="btn-sm btn-success deleteBtn" data-id = "{{ $data->id }}"><i class="fas fa-trash-alt"></i></button>
                    </div>
                </td>
            </tr>
        <?php
            $i += 1;
            }
        ?>
    </tbody>
</table>
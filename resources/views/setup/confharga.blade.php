@extends('index')
@section('judulpage')
    Konfigurasi Harga
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-barcode-scan m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Konfigurasi Harga</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Konfigurasi Harga</h3>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                        <div class="col-md-2" align="right">
                            <button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#ModalTambah" data-whatever="@mdo"><i class="mdi mdi-plus"></i> Tambah </button></div>
                            <!--Modal Tambah-->
                            <div class="modal fade" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <form id="formHarga">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLabel1">Tambah Konfigurasi Harga</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="id" id="idHarga"/>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Tipe</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <select id="kolkolah" name="is_non_resep" class="custom-select custom-select-sm">
                                                            <option value="0">Resep</option>
                                                            <option value="1">Non-Resep</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Nama</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" name="nama" class="namaMargin form-control form-control-sm" placeholder="Nama Margin" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row inputMin">
                                                    <div class="col-md-4"><label class="control-label ">Batas Atas</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="number" name="max" class="form-control form-control-sm maxMargin" placeholder="Max" min="0" step="500">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Margin</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="number" name="margin" class="marginePira form-control form-control-sm" placeholder="Margin" min="0" max="1" step=".01">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="tst3 btn-sm btn-success">Tambah</button>
                                            <button type="button" class="btn-sm btn-inverse" data-dismiss="modal"
                                                onclick="$('#exampleModalLabel1').html('Tambah Konfigurasi Harga');$('#formHarga').trigger('reset');">Cancel</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--End Modal Tambah-->
                    </div>
                    <div class="table-responsive m-t-40">
                        @include('setup.confhargaTable')
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/jquery.alphanum.js')}}"></script>
<script>
    $(function() {
        $(".namaMargin").alphanum({
            allowOtherCharSets: false
        });
        $(".marginePira").alphanum({
            allow: '.'
        });
        $('#myTable').DataTable( {
        "columnDefs": [ {
          "targets": [3],
          "orderable": false,
        }]
        });
        $('#tableNonResep').DataTable({
            "searching": false,
            "paging":   false,
            "ordering": false,
            "info":     false
        });
        $('#kolkolah').on('change', function(){
            console.log($(this).val())
            if($(this).val() == '0'){
                $('.inputMin').hide();
            }else{
                $('.inputMin').show();
            }
        });
        function resetForm(){
            $(".tst3").html("Tambah");
            $('#exampleModalLabel1').html('Tambah Konfigurasi Harga');
            $('#formHarga').trigger('reset');
            $('.inputMin').hide();
        }
        resetForm();
        $('#ModalTambah').on('hidden.bs.modal', function () {
            resetForm();
        })
        $('.table-responsive').on('click', '.editBtn', function(e){
            var id = $(this).data('id');
            $.ajax({
                method: "get",
                url: "{{URL::to('/setup/confharga/data-one')}}/"+id,
                success: function(data){
                    $(".tst3").html("Update");
                    $("#formHarga").populateForm(data);
                    $('#exampleModalLabel1').html('Edit Konfigurasi Harga');
                    $("#ModalTambah").modal('show');
                    if($('#kolkolah').val() == '0'){
                        $('.inputMin').hide();
                    }else{
                        $('.inputMin').show();
                    }
                }
            })
        });
        $('.table-responsive').on('click', '.deleteBtn', function(e){
            var id = $(this).data('id');
            console.log(id)
            swal({
                title: "Anda yakin ingin menghapus data ini?",   
                text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Hapus"
            }).then((willDelete) => {
                if(willDelete["value"]){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: "post",
                        url: "{{URL::to('/setup/confharga/delete')}}",
                        data: "id="+id,
                        success: function(data){
                            if(data){
                                $('.table-response').fadeOut();
                                $('.table-responsive').load("{{URL::to('/setup/confharga/all')}}",function(){
                                    $('.table-response').fadeIn();
                                    $('#myTable').DataTable();
                                });
                                swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
                            }else{
                                swal("Gagal!", "Data Gagal Dihapus.", "error"); 
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    })
                }
            });
        });
        $("#formHarga").on('submit', function(e){
            e.preventDefault()
            if($('.tst3').html() == "Tambah"){
                var tipe = "simpan"
                var url = "{{URL::to('/setup/confharga/insert')}}"
            }else{
                var tipe = "update"
                var url = "{{URL::to('/setup/confharga/update')}}"
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: url,
                data: $("#formHarga").serialize(),
                success: function(data){
                    if(data != false){
                        resetForm();
                        $("#ModalTambah").modal('hide');
                        $('.table-response').fadeOut();
                        $('.table-responsive').load("{{URL::to('/setup/confharga/all')}}",function(){
                            $('.table-response').fadeIn();
                            $('#myTable').DataTable();
                        });
                        $.toast({
                            heading: ' Success Message ',
                            text: 'Konfigurasi Harga Berhasil Di'+tipe,
                            position: 'top-right',
                            loaderBg:'#ffffff',
                            icon: 'success'
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
    });
</script> 
@endsection
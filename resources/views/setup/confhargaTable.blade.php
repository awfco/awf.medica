<h3>Harga Non-Resep</h3>
<table id="tableNonResep" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Max</th>
            <th>Margin</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $total = count($hargaNonResep);
        $lastmargin = 0;
        $lastname = "Batas Akhir";
        $lastmax = 0;
        foreach($hargaNonResep as $data){
                ?>
        <tr>
            <td class="isijal">{{ $i }}</td>
            <td class="isijal">{{ $data->nama }}</td>
            <td class="isijal">{{ $data->max }}</td>
            <td class="isijal">{{ $data->margin }}</td>
            <td class="isijal">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn-sm btn-success editBtn" data-id="{{ $data->id }}"><i class="mdi mdi-pencil"></i></button>
                    <button type="button" class="btn-sm btn-success deleteBtn" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></button>
                </div>
            </td>
        </tr>
        <?php
            $i += 1;
            $lastmax = $data->max;
            $lastmargin = $data->margin;
        }
        ?>
        <tr>
            <td class="isijal">{{ $i }}</td>
            <td class="isijal">{{ $lastname }}</td>
            <td class="isijal">&gt; {{ $lastmax }}</td>
            <td class="isijal">{{ $lastmargin }}</td>
            <td class="isijal">&nbsp;
            </td>
        </tr>
    </tbody>
</table>
<br/>
<h3>Harga Resep</h3>
<table id="myTable" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Margin</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $i = 1;
            foreach($confharga as $data){
        ?>
        <tr>
            <td class="isijal">{{ $i }}</td>
            <td class="isijal">{{ $data->nama }}</td>
            <td class="isijal">{{ $data->margin }}</td>
            <td class="isijal">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn-sm btn-success editBtn" data-id="{{ $data->id }}"><i class="mdi mdi-pencil"></i></button>
                    <button type="button" class="btn-sm btn-success deleteBtn" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></button>
                </div>
            </td>
        </tr>
        <?php
                $i += 1;
            }
        ?>
    </tbody>
</table>
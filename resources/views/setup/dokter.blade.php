@extends('index')
@section('judulpage')
    Data Dokter
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-stethoscope m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Data Dokter</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Data Dokter</h3>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                        <div class="col-md-2" align="right">
                            <button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="mdi mdi-plus"></i> Tambah </button>
                        </div>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <form id = "formDokter">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleModalLabel1">Tambah Data Dokter</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body">
                                            <input type="hidden" name="id" id="idDokter"/>
                                            <div class="form-group row">
                                                <div class="col-md-4"><label class="control-label ">Nama Dokter</label></div>
                                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                                <div class="col-md">
                                                    <input type="text" class="namaDokter form-control form-control-sm" placeholder="Nama Dokter" name="nama" autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"><label class="control-label ">Spesialis</label></div>
                                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                                <div class="col-md">
                                                    <input type="text" class="spesialisDokter form-control form-control-sm" placeholder="Spesialis" name="spesialis" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"><label class="control-label ">Alamat</label></div>
                                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                                <div class="col-md">
                                                    <input type="text" class="alamatDokter form-control form-control-sm" placeholder="Alamat" name="alamat" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"><label class="control-label ">No. Telp</label></div>
                                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                                <div class="col-md">
                                                    <input type="text" class="noDokter form-control form-control-sm" placeholder="No. Telp" name="notelp">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4"><label class="control-label ">Jenis Margin</label></div>
                                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                                <div class="col-md">
                                                    <select class="custom-select custom-select-sm" name="conf_harga_id">
                                                        <option value="">Pilih Jenis Margin</option>
                                                        <?php foreach($confharga as $conf){ ?>
                                                            <option value="{{$conf->id}}">{{$conf->nama}}</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="tst3 btn-sm btn-success">Tambah</button>
                                        <button type="button" class="btn-sm btn-inverse" data-dismiss="modal" 
                                            onclick="$('#exampleModalLabel1').html('Tambah Data Dokter');$('#formDokter').trigger('reset');">Cancel</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive m-t-40">
                        @include('setup.dokterTable')
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/jquery.alphanum.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable( {
        "columnDefs": [ {
          "targets": [6],
          "orderable": false,
        }]
        });
        $(".namaDokter").alpha({
            allow: '.,'
        });
        $(".spesialisDokter").alpha();
        $(".noDokter").numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false
        });
        var tables = $('#myTable').DataTable();
        function resetForm(){
            $(".tst3").html("Tambah");
            $('#exampleModalLabel1').html('Tambah Data Dokter');
            $('#formDokter').trigger('reset');
            $('#idDokter').val("")
        }
        $('#exampleModal').on('hidden.bs.modal', function () {
            resetForm();
        })
        $('.table-responsive').on('click', '.editBtn', function(e){
            var id = $(this).data('id');
            $.ajax({
                method: "get",
                url: "{{URL::to('/setup/dokter/data-one')}}/"+id,
                success: function(data){
                    console.log(data);
                    $(".tst3").html("Update");
                    $("#formDokter").populateForm(data);
                    $("#exampleModalLabel1").html('Edit Data Dokter');
                    $("#exampleModal").modal('show');
                }
            })
        });
        $('.table-responsive').on('click', '.deleteBtn', function(e){
            var id = $(this).data('id');
            console.log(id)
            swal({
                title: "Anda yakin ingin menghapus data ini?",   
                text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Hapus"
            }).then((willDelete) => {
                if(willDelete["value"]){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: "post",
                        url: "{{URL::to('/setup/dokter/delete')}}",
                        data: "id="+id,
                        success: function(data){
                            if(data){
                                $('.table-response').fadeOut();
                                $('.table-responsive').load("{{URL::to('/setup/dokter/all-dokter')}}",function(){
                                    $('.table-response').fadeIn();
                                    $('#myTable').DataTable();
                                });
                                swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
                            }else{
                                swal("Gagal!", "Data Gagal Dihapus.", "error"); 
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    })
                }
            });
        });
        $("#formDokter").on('submit', function(e){
            e.preventDefault()
            if($("#idDokter").val() == ""){
                var tipe = "simpan"
                var url = "{{URL::to('/setup/dokter/insert')}}"
            }else{
                var tipe = "update"
                var url = "{{URL::to('/setup/dokter/update')}}"
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: url,
                data: $("#formDokter").serialize(),
                success: function(data){
                    if(data != false){
                        resetForm();
                        $("#exampleModal").modal('hide');
                        $('.table-response').fadeOut();
                        $('.table-responsive').load("{{URL::to('/setup/dokter/all-dokter')}}",function(){
                            $('.table-response').fadeIn();
                            $('#myTable').DataTable();
                        });
                        $.toast({
                            heading: ' Success Message ',
                            text: 'Data '+data.nama+' Berhasil Di'+tipe,
                            position: 'top-right',
                            loaderBg:'#ffffff',
                            icon: 'success'
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
    });
    $(function() {
      $(".tstupdate").click(function(){
           $.toast({
            heading: ' Success Message ',
            text: 'Data Dokter Berhasil Diupdate',
            position: 'top-right',
            loaderBg:'#ffffff',
            icon: 'success'
          });
     });
    });
</script> 
@endsection
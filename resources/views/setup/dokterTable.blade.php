
<table id="myTable" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th>No</th>
            <th width="20%">Nama</th>
            <th>Spesialis</th>
            <th>Alamat</th>
            <th width="10%">No. Telp</th>
            <th width="5%">Margin</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 1;
            foreach($dokter as $data){ 
        ?>
        <tr>
            <td class="isijal">{{ $i }}</td>
            <td class="isijal">{{ $data->nama }}</td>
            <td class="isijal">{{ $data->spesialis }}</td>
            <td class="isijal">{{ $data->alamat }}</td>
            <td class="isijal">{{ $data->notelp }}</td>
            <td class="isijal">{{ $data->confHarga->nama }}</td>
            <td class="isijal">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn-sm btn-success editBtn" data-id="{{ $data->id }}"><i class="mdi mdi-pencil"></i></button>
                    <button type="button" class="btn-sm btn-success deleteBtn" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></button>
                </div>
            </td>
        </tr>
        <?php 
                $i += 1;
            } 
        ?>
    </tbody>
</table>
@extends('index')
@section('judulpage')
    Stok Opname
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-layers m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Stok Opname</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Stok Opname</h3>
                    <br/>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Kode Obat</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md"><p class="form-control-static">KD-001</p></div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Stok Gudang (Sistem)</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md"><p class="form-control-static">70</p></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Nama Obat</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md"><p class="form-control-static">Amoxcillin</p></div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Stok Gudang (Real)</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md"><input class="form-control form-control-sm" type="number" value="0" id="example-number-input" style="font-size: small; width:50px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Selisih</label></div>
                                <div class="col-md-1"><label class="control-label ">:</label></div>
                                <div class="col-md"><p class="form-control-static">-1</p></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md-5"><label class="control-label ">Alasan</label></div>
                                <div class="col-md"></div>
                                <div class="col-md"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md">
                            <div class="form-group row">
                                <div class="col-md"><textarea class="form-control gudang"></textarea></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md" align="right">
                    <button type="button" class="tst3 btn-sm btn-success"><i class="fas fa-arrow-up"></i> Update </button>
                    </div>
                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-hover">
                            <thead align="center">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Obat</th>
                                    <th>Nama Obat</th>
                                    <th>Stok Sistem Gudang</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>KD-001</td>
                                    <td>Amoxcillin</td>
                                    <td>70</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script>
    $(function() {
        $('#myTable').DataTable();
    });
    $('#sa-warning').click(function(){
        swal({   
            title: "Anda yakin ingin menghapus data ini?",   
            text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Hapus",   
            closeOnConfirm: false 
        }, function(){   
            swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
        });
    });
    $(function() {
      $(".tst3").click(function(){
           $.toast({
            heading: ' Success Message ',
            text: 'Data Berhasil Terupdate !',
            position: 'top-right',
            loaderBg:'#ffffff',
            icon: 'success'
          });
     });
    });
</script> 
@endsection
<table id="myTable" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th></th>
            <th>No</th>
            <th width="20%">Nama</th>
            <th width ="5%">Usia</th>
            <th width="15%">No. Telp</th>
            <th>Alamat</th>
            <th width="20%">Dokter</th>
            <th class="no-sort">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $i = 1;
            foreach($pasien as $data){
        ?>
        <tr data-child-value="{{ $data->ket }}">
            <td class="details-control"></td>
            <td class="isijal">{{ $i }}</td>
            <td class="isijal">{{ $data->nama }}</td>
            <td class="isijal">{{ $data->usia }}</td>
            <td class="isijal">{{ $data->notelp }}</td>
            <td class="isijal">{{ $data->alamat }}</td>
            <td class="isijal">{{ $data->dokter->nama }}</td>
            <td class="isijal">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn-sm btn-success editBtn" data-id="{{ $data->id }}"><i class="mdi mdi-pencil"></i></button>
                    <button type="button" class="btn-sm btn-success deleteBtn" data-id="{{ $data->id }}"><i class="fas fa-trash-alt"></i></button>
                </div>
            </td>
        </tr>
        <?php
                $i++;
            }
        ?>
    </tbody>
</table>
@extends('index')
@section('judulpage')
    Profil Apotek
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <img src="{{asset('/img/apotekicon.png')}}" width="23px" height="23px"/>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Setup Profile Apotek</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Setup Profile Apotek</h3>
                    <div class="row" style="padding:1.25rem">
                        <div class="col-md-4 bgprofile" style="text-align:center">
                            <div class="imgProfile">
                                <div style="width:150px;height:150px;position:absolute">
                                    <form id="changePP">
                                        <input class="formpp" type="file" name="pp" style="display:none" accept="image/*"/>
                                    </form>
                                    <img class="plusImage" src="{{asset('/img/plusimg.png')}}" height="30px" width="30px"/>
                                    <img class="profilePictApotek" src="{{(($apotek && $apotek->logo != '')) ? URL::to($apotek->logo) : asset('/img/ppapotekdefault.png')}}" width="150px" height="150px"/>
                                </div>
                            </div>
                            <h3>Setup your apotek</h3>
                            <span style="margin-top:15px">Let's know a little bit about your apotek<br/>This won't take long</span>
                        </div>
                        <div class="col-md-4" style="margin-left:50px;">
                            <form class="form-horizontal form-material" id="profileApotekForm">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Nama Apotik</label>
                                        <input name="nama" class="form-control" type="text" placeholder="Nama Apotek" value="{{($apotek) ? $apotek->nama : ''}}">
                                        <i class="fa fa-exclamation alertnama">
                                            <div class="bubble">
                                                <div class="message">
                                                    <div class="title">Nama Apotek</div>
                                                    Nama apotek, akan ditampilkan pada bagian atas nota
                                                </div>
                                            </div>
                                        </i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Alamat</label>
                                        <textarea name="alamat" class="form-control" type="text" rows=4>{{($apotek) ? $apotek->alamat : ''}}</textarea>
                                        <i class="fa fa-exclamation khusus alertalamat">
                                            <div class="bubble">
                                                <div class="message">
                                                    <div class="title">Alamat</div>
                                                    Lokasi apotek, akan ditampilkan pada bagian atas nota
                                                </div>
                                            </div>
                                        </i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>No Telp</label>
                                        <input name="notelp" class="form-control" type="text" placeholder="No Telp" value="{{($apotek) ? $apotek->notelp : ''}}">
                                        <i class="fa fa-exclamation alertnotelp">
                                            <div class="bubble">
                                                <div class="message">
                                                    <div class="title">No Telp</div>
                                                    No. kontak apotek, akan ditampilkan pada bagian atas nota
                                                </div>
                                            </div>
                                        </i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Keterangan Nota</label>
                                        <input name="footernota" class="form-control" type="text" placeholder="Keterangan Nota" value="{{($apotek) ? $apotek->footernota : ''}}">
                                        <i class="fa fa-exclamation alertfooternota">
                                            <div class="bubble">
                                                <div class="message">
                                                    <div class="title">Keterangan Nota</div>
                                                    Data untuk ditampilkan dibawah nota
                                                </div>
                                            </div>
                                        </i>
                                    </div>
                                </div>
                                <div class="row rowbutton">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12 align-self-end">
                                                <button type="reset" class="btn-sm btn-success btncancel">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-xs-12 align-self-end">
                                                <button class="btn-sm btn-success btnsave">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/jquery.alphanum.js')}}"></script>
<script>
    $(function() {
        var formnya = $("#profileApotekForm")
        @if(Session::has('msg'))
            swal({
                title: "Input data apotek",   
                text: "{{Session::get('msg')}}",   
                type: "warning",   
                confirmButtonColor: "#DD6B55"
            })
        @endif
        $(".plusImage").on("click", function(){
            $(".formpp").trigger("click");
        })
        $(".formpp").on('change', function(){
            var form = $('#changePP')[0];
            var data = new FormData(form);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                processData: false, 
                contentType: false,
                cache: false,
                url: "{{URL::to('setup/apotek/changepp')}}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                success: function (res) {
                    result = JSON.parse(res)
                    if(result.success){
                        $(".profilePictApotek").attr('src',result.filename+"?"+(new Date()))
                            $.toast({
                                heading: ' Success Message ',
                                text: 'Logo apotek berhasil di update',
                                position: 'top-right',
                                loaderBg:'#ffffff',
                                icon: 'success'
                            });
                    }else{
                        $.toast({
                            heading: ' Error Message ',
                            text: result.msg,
                            position: 'top-right',
                            loaderBg:'#ff0000',
                            icon: 'error'
                        });
                    }
                }
            })
        })
        function validasi(){
            var passed = true;
            $('#profileApotekForm>input').each(function(i, obj){
                nama = $(obj).attr("name")
                if($(obj).val() == ""){
                    $(".alert"+nama).show();
                    passed = false
                }else{
                    $(".alert"+nama).hide();
                }
            })
            if($('textarea').val() == ""){
                $(".alertalamat").show();
                passed = false
            }else{
                $(".alertalamat").hide();
            }
            return passed
        }
        $(".btnsave").on('click', function(){
            if(validasi()){
                $.ajax({
                    type: 'POST',
                    url: "{{URL::to('setup/apotek/change')}}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formnya.serialize(),
                    success: function(result){
                        res = JSON.parse(result)
                        if(res.success){
                            $.toast({
                                heading: ' Success Message ',
                                text: 'Profil apotek berhasil di update',
                                position: 'top-right',
                                loaderBg:'#ffffff',
                                icon: 'success'
                            });
                        }else{
                            $.toast({
                                heading: 'Error Message',
                                text: res.msg,
                                position: 'top-right',
                                loaderBg:'#ff0000',
                                icon: 'error'
                            });
                        }
                    }
                })
            }
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;   
        })
    });
</script> 
@endsection
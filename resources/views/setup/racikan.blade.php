@extends('index')
@section('judulpage')
    Data Racikan
@endsection
@section('css')
    <link href="{{asset('/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/dataTables.bootstrap4.css')}}">
    <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/css/select.dataTables.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles bgtgh">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor"><i class = "mdi mdi-pill m-b-0 warnaoren"></i></h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" class="warnabirumuda">Home</a></li>
                        <li class="breadcrumb-item">Setup</li>
                        <li class="breadcrumb-item active">Data Racikan</li>
                    </ol>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title m-b-5"><span class="lstick warnaorenbg"></span>Data Racikan</h3>
                    <div class="row">
                        <div class="col-md"></div>
                        <div class="col-md"></div>
                        <div class="col-md-2" align="right"><button type="button" class="btn-sm btn-success" data-toggle="modal" data-target="#ModalTambah" data-whatever="@mdo"><i class="mdi mdi-plus"></i> Tambah </button></div>
                            <div class="modal fade long-modal" id="ModalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <form id="formRacikan">
                                            <input type="hidden" name="id" id="idRacikan"/>
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">Tambah Data Racikan</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Nama Racikan</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="form-control form-control-sm namaRacikan" placeholder="Nama Racikan" name="nama" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Dokter</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <select class="select2" style="width: 100%" name="dokter_id">
                                                                <option value="">Pilih Dokter</option>
                                                                @foreach($dokter as $dDokter)
                                                                <option value="{{ $dDokter->id }}">{{ $dDokter->nama }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Tipe Racikan</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio1" name="jenis" value="Statis" type="radio" class="custom-control-input" checked>
                                                                <span class="custom-control-label">Statis</span>
                                                            </label>
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio2" name="jenis" value="Dinamis" type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">Dinamis</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Fee Dokter</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="fee harga form-control form-control-sm" placeholder="Fee Dokter" name="fee_dokter" min="0" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Fee Kapsul</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="text" class="fee harga form-control form-control-sm" placeholder="Fee Kapsul" name="fee_kap" min="0" required>
                                                    </div>
                                                </div>
                                                <div class="jmlkapsul form-group row">
                                                    <div class="col-md-4"><label class="control-label ">Jumlah Kapsul</label></div>
                                                    <div class="col-md-1"><label class="control-label ">:</label></div>
                                                    <div class="col-md">
                                                        <input type="number" class="jml_kapsul form-control form-control-sm" placeholder="Jumlah Kapsul" name="jml_kapsul" min="1" required>
                                                    </div>
                                                </div>
                                                <div class="table-responsive tableDetailRacikan">
                                                    <table class="table table-bordered" style="font-size: small">
                                                            <thead>
                                                                <tr align="center"> 
                                                                    <th>Kode</th>
                                                                    <th>Nama</th>
                                                                    <th>Satuan</th>
                                                                    <th>Jenis</th>
                                                                    <th>Harga</th>
                                                                    <th>Qty</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="listObat">
                                                                <tr align="center" class="row0">
                                                                <input type="hidden" name="detail_barang_id[]" class="obatId0" value=""/>
                                                                <input type="hidden" name="detail_id[]" class="detailId0" value=""/>
                                                                <input type="hidden" name="detail_qty[]" class="obatQty0" value=""/>
                                                                    <td class="kodeObat0"></td>
                                                                    <td width="15%" class="namaObat0">
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control form-control-sm" placeholder="Cari" style="font-size: small">
                                                                            <div class="input-group-append">
                                                                                <button class="btn-sm btn-success" type="button" data-toggle="modal" data-target="#modalSearchBarang" data-whatever="@mdo"><i class="mdi mdi-magnify"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="satuanObat0"></td>
                                                                    <td class="jenisObat0"></td>
                                                                    <td class="hargaObat0"></td>
                                                                    <td width="10%" class="qty0">
                                                                        <input class="form-control form-control-sm" type="number" value="0" id="example-number-input" style="font-size: small">
                                                                    </td>
                                                                    <td><div class="btn-group btnActionObat0" role="group" aria-label="Basic example" class="action0">
                                                                            <button type="button" class="btn-sm btn-success btnAddRow"><i class="mdi mdi-plus"></i></button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="tst3 btn-sm btn-success">Tambah</button>
                                                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="table-responsive listRacikan m-t-40">
                        @include('setup.racikanTable')
                    </div>
                </div>
            </div>

<div class="modal fade long-modal" id="modalSearchBarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1" align="center">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive m-t-40">
                    <table id="myTable2" class="table table-bordered table-hover">
                        <thead align="center">
                            <tr>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Jenis</th>
                                <th>Satuan</th>
                                <th>Stok</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($barang as $datBar)
                            <tr>
                                <td class="isijal"> {{$datBar->id}} </td>
                                <td class="isijal"> {{ $datBar->nama }} </td>
                                <td class="isijal"> {{ $datBar->jenis }} </td>
                                <td class="isijal"> {{ $datBar->satuan }} </td>
                                <td class="isijal"> {{ $datBar->stock }} </td>
                                <td class="isijal"> {{ $datBar->total() }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-inverse" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('/js/sweetalert.min.js')}}"></script>
<script src="{{asset('/js/jquery.toast.js')}}"></script>
<script src="{{asset('/js/datatables.min.js')}}"></script>
<script src="{{asset('/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/jquery.alphanum.js')}}"></script>
<script>
    $(function() {
        $(".fee").alphanum();
        $(".jml_kapsul").alphanum({
            allow: '.'
        });
        $(".harga").numeric({
            allowMinus: false,
            allowThouSep: false,
            allowDecSep: false
        });
        $('#myTable').DataTable( {
        "columnDefs": [ {
          "targets": [5],
          "orderable": false,
        }]
        });
        var currRow = 0
        function emptyRow(count){ return `
            <tr align="center" class="row${count}">
                <input type="hidden" name="detail_barang_id[]" class="obatId${count}" value=""/>
                <input type="hidden" name="detail_id[]" class="detailId${count}" value=""/>
                <input type="hidden" name="detail_qty[]" class="obatQty${count}" value=""/>
                <td class="kodeObat${count}"></td>
                <td width="15%" class="namaObat${count}">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-sm" placeholder="Cari" style="font-size: small">
                        <div class="input-group-append">
                            <button class="btn-sm btn-success" type="button" data-toggle="modal" data-target="#modalSearchBarang" data-whatever="@mdo"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>
                </td>
                <td class="satuanObat${count}"></td>
                <td class="jenisObat${count}"></td>
                <td class="hargaObat${count}"></td>
                <td width="10%" class="qty${count}">
                    <input class="form-control form-control-sm" type="number" value="0" id="example-number-input" style="font-size: small">
                </td>
                <td><div class="btn-group btnActionObat${count}" role="group" aria-label="Basic example">
                        <button type="button" class="btn-sm btn-success btnAddRow"><i class="mdi mdi-plus"></i></button>
                    </div>
                </td>
            </tr>
        ` }
        function editRow(count){
            oldQty = $('.qty'+count).html()
            $('.qty'+count).html(`
                <input class="form-control form-control-sm" type="number" value="${oldQty}" id="example-number-input" style="font-size: small">
            `)
            $('.btnActionObat'+count).html(`
                <button type="button" class="btn-sm btn-success btnAddRow" data-id="${count}"><i class="mdi mdi-plus"></i></button>
            `)
        }
        function addNewRow(cRow){
            qtyrow = $(".qty"+cRow+" input").val()
            $(".namaObat"+cRow).html( $(".namaObat"+cRow+" input").val() )
            $(".qty"+cRow).html($(".qty"+cRow+" input").val())
            $(".obatQty"+cRow).val(qtyrow)
            $(".btnActionObat"+cRow).html(`
                <button type="button" class="btn-sm btn-success editBtnObat" data-id="${currRow}"><i class="mdi mdi-pencil"></i></button>
                <button id="sa-warning" type="button" class="btn-sm btn-success deleteBtnObat" data-id="${currRow}"><i class="fas fa-trash-alt"></i></button>
            `)
            currRow += 1
            $("#listObat").append(emptyRow(currRow))
        }
        function newRow(){
            if(typeof $('.btnAddRow').data('id') !== 'undefined'){
                cRow = $('.btnAddRow').data('id')
            }else{
                cRow = currRow
            }
            if($('.obatId'+cRow).val() == ""){
                swal("Data tidak lengkap", "Harap isi obat terlebih dahulu sebelum menambahkan baris", "error"); 
            }else if($(".qty"+cRow+" input").val() <= 0){
                swal("Data tidak lengkap", "Harap isi qty > 0", "error"); 
            }else{
                addNewRow(cRow)
                $(".namaObat"+currRow+" button").focus()
            }
        }
        var table1 = $('#myTable').DataTable(); 
        var table2 = $('#myTable2').DataTable({
            select: {
                style: 'single'
            },
            info: false
        }); 
        function selectedRow(){
            dataRow = table2.row({ selected: true }).data()
            for(row = 0; row < currRow; row++){
                if($('.obatId'+row).val() == dataRow[0]){
                    editRow(row)
                    $('.row'+currRow).remove()
                    $('.qty'+row+' input').focus()
                    currRow--
                    $(this).val('')
                    $('#modalSearchBarang').modal('hide')
                    table2.search("").draw()
                    return;
                }
            }
            $(this).val('')
            $('.obatId'+currRow).val(dataRow[0])
            $('.kodeObat'+currRow).html(dataRow[0])
            $('.satuanObat'+currRow).html(dataRow[3])
            $('.jenisObat'+currRow).html(dataRow[2])
            $('.hargaObat'+currRow).html(dataRow[5])
            $(".namaObat"+currRow+" input").val(dataRow[1])
            $('#modalSearchBarang').modal('hide')
            table2.search("").draw()
        }
        $(".select2").select2();
        function resetForm(){
            $("#exampleModalLabel1").html('Tambah Data Racikan')
            $(".tst3").html('Tambah');
            $('#formRacikan').trigger('reset');
            $(".select2").val('').trigger('change')
            $('.nilai').html('')
            for(row = 0; row < currRow; row++){
                $('.row'+row).remove()
            }
            $('.tableDetailRacikan').show()
        }
        $('input[name="jenis"]').on('change', function(e){
            if($(this).val() == "Statis"){
                $('.tableDetailRacikan').show()
                $('.jmlkapsul').show()
            }else{
                $('.tableDetailRacikan').hide()
                $('.jmlkapsul input').val(1)
                $('.jmlkapsul').hide()
            }
        })
        $('#ModalTambah').on('hidden.bs.modal', function () {
            resetForm();
        })
        $('#modalSearchBarang').on('hidden.bs.modal', function() {
            $('body').addClass('modal-open')
        })
        $('#ModalTambah').on('shown.bs.modal', function () {
            $('.namaRacikan').focus();
        })
        $('#modalSearchBarang').on('shown.bs.modal', function(){
            $('div.dataTables_filter input', table2.table().container()).focus();
            table2.row({ filter : 'applied'}, 0).select()
        })
        $('#myTable2 tbody').on( 'click', 'tr', function () {
            if($(this).hasClass('selected')){
                selectedRow()
            }
        });
        $('div.dataTables_filter input', table2.table().container()).keyup( function (e) {
            if (e.keyCode == 13) {
                selectedRow()
            }else{
                table2.row({ filter : 'applied'}, 0).select()
            }
        } );
        $('.listRacikan').on('click', '.editBtn', function(e){
            var id = $(this).data('id');
            $.ajax({
                method: "get",
                url: "{{URL::to('/setup/racikan/data-one')}}/"+id,
                success: function(data){
                    $(".tst3").html("Update");
                    console.log(data)
                    currRow = 0
                    $("#listObat").html(emptyRow(currRow))
                    data.detail_racikan.forEach(function(d){
                        $(".qty"+currRow+" input").val(d.qty)
                        $(".namaObat"+currRow+" input").val(d.barang.nama)
                        $(".obatId"+currRow).val(d.barang_id)
                        $(".detailId"+currRow).val(d.id)
                        $('.kodeObat'+currRow).html(d.barang.id)
                        $('.satuanObat'+currRow).html(d.barang.satuan)
                        $('.jenisObat'+currRow).html(d.barang.jenis)
                        $('.hargaObat'+currRow).html(d.barang.harga)
                        addNewRow(currRow)
                    })
                    $("#formRacikan").populateForm(data);
                    $(".select2").val(data.dokter_id).trigger('change');
                    $('#exampleModalLabel1').html('Edit Data Racikan');
                    $("#ModalTambah").modal('show');
                    if(data.jenis == "Statis"){
                        $('.tableDetailRacikan').show()
                        $('.jmlkapsul').show()
                    }else{
                        $('.tableDetailRacikan').hide()
                        $('.jmlkapsul').hide()
                    }
                }
            })
        });
        $('.listRacikan').on('click', '.deleteBtn', function(e){
            var id = $(this).data('id');
            swal({
                title: "Anda yakin ingin menghapus data ini?",   
                text: "Data yang sudah terhapus tidak dapat kembali lagi !",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Hapus"
            }).then((willDelete) => {
                if(willDelete["value"]){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: "post",
                        url: "{{URL::to('/setup/racikan/delete')}}",
                        data: "id="+id,
                        success: function(data){
                            if(data){
                                $('.listRacikan').load("{{URL::to('/setup/racikan/all')}}",function(){
                                    $('#myTable').DataTable();
                                });
                                swal("Terhapus!", "Data Berhasil Dihapus.", "success"); 
                            }else{
                                swal("Gagal!", "Data Gagal Dihapus.", "error"); 
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    })
                }
            });
        });
        $("#formRacikan").on('submit', function(e){
            e.preventDefault()
            if($(".tst3").html() == "Tambah"){
                var tipe = "simpan"
                var url = "{{URL::to('/setup/racikan/insert')}}"
            }else{
                var tipe = "update"
                var url = "{{URL::to('/setup/racikan/update')}}"
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: url,
                dataType : 'json',
                data: $("#formRacikan").serialize(),
                success: function(data){
                    if(data != false){
                        resetForm();
                        $("#ModalTambah").modal('hide');
                        $('.listRacikan').fadeOut();
                        $('.listRacikan').load("{{URL::to('/setup/racikan/all')}}",function(){
                            $('.listRacikan').fadeIn();
                            $('#myTable').DataTable();
                        });
                        $.toast({
                            heading: ' Success Message ',
                            text: 'Racikan Berhasil Di'+tipe,
                            position: 'top-right',
                            loaderBg:'#ffffff',
                            icon: 'success'
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            })
        });
        $("#listObat").on('click', '.btnAddRow', newRow)
        $("#listObat").on('click', '.editBtnObat', function(){
            editRow($(this).data('id'))
            $('.row'+currRow).remove()
            currRow--
        })
        $("#listObat").on('click', '.deleteBtnObat', function(){
            var id = $('.detailId'+$(this).data('id')).val()
            if(id != ""){
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: "post",
                    url: "{{URL::to('/setup/racikan/delete-detail')}}",
                    data: "id="+id,
                    success: function(data){
                        if(data){
                            $('.row'+$(this).data('id')).remove();
                            currRow--
                        }else{
                            return false;
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                })
            }
            $('.row'+$(this).data('id')).remove();
        });
    });
</script> 
@endsection
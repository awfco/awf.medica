<table id="myTable" class="table table-bordered table-hover">
    <thead align="center">
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama Supplier</th>
            <th>Nama Sales</th>
            <th>Jenis</th>
            <th>Alamat</th>
            <th>Kota</th>
            <th>No. Telp</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
            <tr>
                <td class="isijal">1</td>
                <td class="isijal">TK-121-ASD</td>
                <td class="isijal">Kimia Farma</td>
                <td class="isijal">Baldum</td>
                <td class="isijal">PBF</td>
                <td class="isijal">Jl. Pancurawis</td>
                <td class="isijal">Purwokerto</td>
                <td class="isijal">0281634315</td>
                <td class="isijal">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn-sm btn-success editBtn"><i class="mdi mdi-pencil"></i></button>
                        <button id="sa-warning" type="button" class="btn-sm btn-success deleteBtn"><i class="fas fa-trash-alt"></i></button>
                    </div>
                </td>
            </tr>
    </tbody>
</table>